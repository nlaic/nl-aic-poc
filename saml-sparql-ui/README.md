# SAML SPARQL UI

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 0.1.0](https://img.shields.io/badge/AppVersion-0.1.0-informational?style=flat-square)

This repository contains a Vue application that acts as the front-end for the Sparql query consumer.

>*NOTE:* This project is part of a Proof of Concept and therefore does not contain production ready code!

## Project setup
```
cd api
npm install
cd gui
npm install
```

### Compiles and hot-reloads for development

```
cd api
node server.js
cd gui
npm run serve
```

### Docker Image usage

```
docker run -it --rm -p 8080:80 -e DATA_APP_ENDPOINT=<Endpoint of the Sparql Consuming Data App API> -e IDS_PROVIDERS=<; seperated urn's for the IDS connectors hosting the Sparql service> -e SAML_PROVIDERS <URL's for the SAML connectors hosting the Sparql service>  registry.ids.smart-connected.nl/saml-sparql-ui
```

## Configuration
The Saml-Sparql-UI needs the following environment variables set to work properly

| Key               | Description                                                           |
| ----------------- | --------------------------------------------------------------------- |
| DATA_APP_ENDPOINT | Endpoint of the Sparql Consuming Data App API                         |
| IDS_PROVIDERS     | urn's for the IDS connectors hosting the Sparql service               |
| SAML_PROVIDERS    | URL's for the SAML connectors hosting the Sparql service              |

## Usage

The UI contains 2 tabs which enble the user to define either a SPARQL query manually of use a predefined query which can be created using the available filters.

<img src="img/UI1.png" width=50%><img src="img/UI2.png" width=50%>

In order to send a query a provider has to be selected. When a SAML typed provider is selected the user first has to with the SAML IdP before a qeury can be send to this provider. In this demo the follwing credentials are used through configuration of the SAML IdP :

- user: test_user
- password: test password  (notice the space between test and password)

<img src="img/SAML_login.png" width=50%>

After the login is successfull the button <img src="img/bt_login.png" width="75px"> will change into  <img src="img/bt_logout.png" width="75px">

When options are choosen and the query has been send the result will be shown in a dynamic table

<img src="img/UI3.png" width=50%>

The raw repsonse from the provider van be viewed by clicking on <img src="img/bt_showhide.png" width="75xp">

<img src="img/raw_response.png" width="50%">
