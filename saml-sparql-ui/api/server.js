const express = require('express');
const axios = require('axios');
const path = require('path');
const randomId = require('random-id');
const app = express(),
    bodyParser = require("body-parser");

const dataAppEndpoint = process.env.DATA_APP_ENDPOINT || ""
const idsProviders = process.env.IDS_PROVIDERS || ""
const samlProviders = process.env.SAML_PROVIDERS || ""

port = 8080;

let providers = {}
idsProviders.split(';').forEach(provider => {
    if (provider.trim() !== '') {
        providers[provider] = {
            type: 'IDS',
        }
    }
})
samlProviders.split(';').forEach(provider => {
    if (provider.trim() !== '') {
        providers[provider] = {
            type: 'SAML',
            token: undefined
        }
    }
});

app.use(function(req, res, next) {
    if (req.query.token !== undefined && req.query.provider !== undefined) {
        providers[req.query.provider].token = req.query.token
        console.log(`Successfully set token to ${req.query.token} for provider ${req.query.provider}`)
        res.redirect('/')
    } else {
        next()
    }
})

app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, '../gui/dist')));

app.get('/providers', async(req, res) => {
    res.send(providers)
})

app.post('/logout', async(req, res) => {
    const provider = req.body.provider
    if (providers[provider] != undefined) {
        providers[provider].token = undefined;
    }
    res.send("logged out")
})

app.post('/sparql', async(req, res) => {

    const provider = req.body.provider;
    const query = req.body.query;
    const dataset = req.body.dataset;
    const format = req.body.format;
    const options = {};
    let url;
    if (providers[provider] !== undefined) {
        switch (providers[provider].type) {
            case 'IDS':
                url = `${dataAppEndpoint}/sparql`
                options.params = {
                    provider: provider,
                    query: query,
                    format: format,
                    dataset: dataset
                }
                break;
            case 'SAML':
                url = `https://${provider}/saml/sparql`
                options.params = {
                    provider: provider,
                    query: query,
                    format: format,
                    dataset: dataset
                }
                if (providers[provider].token !== undefined) {
                    options.headers = {
                        'Authorization': `Bearer ${providers[provider].token}`
                    }
                }
                break;
        }
    }
    try {
        console.log("forwarding to : " + url);
        console.log("with options : " + JSON.stringify(options));
        console.log("-------------------------------------------");
        const data = await axios.get(url, options)
        res.set('Content-Type', data.headers['content-type'] || 'text/plain');
        res.send(data.data);
    } catch (error) {
        if (error.response) {
            res.status(error.response.status);
            res.send(error.response.data);
        } else {
            res.status(400);
            res.send();
            console.log(error)
        }
    }
});

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, '../gui/dist/index.html'));
});

app.listen(port, () => {
    console.log(`Server listening on the port::${port}`);
});