package nl.tno.ids.aic.fl.worker

import de.fraunhofer.iais.eis.InvokeOperationMessageBuilder
import de.fraunhofer.iais.eis.ParameterAssignmentBuilder
import de.fraunhofer.iais.eis.util.Util
import nl.tno.ids.aic.fl.DataAppUtils
import nl.tno.ids.aic.fl.DatasetController
import nl.tno.ids.aic.fl.FederatedLearningConfig
import nl.tno.ids.base.IDSRestController
import nl.tno.ids.common.serialization.Config
import nl.tno.ids.common.serialization.DateUtil
import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.StringEntity
import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.*
import java.net.URI
import javax.annotation.PostConstruct
import javax.servlet.http.HttpServletResponse

import org.apache.jena.sparql.engine.http.QueryEngineHTTP
import org.apache.jena.query.ResultSetFormatter
import org.apache.jena.query.QueryExecutionFactory
import org.apache.jena.query.QueryFactory

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule

import java.io.ByteArrayOutputStream;

@RestController
@RequestMapping("saml")
class SAMLSparqlController {
  private val mapper = ObjectMapper().registerModule(KotlinModule())
  private var config: FederatedLearningConfig = FederatedLearningConfig()
  
  @PostConstruct
  fun init() {
    config = Config.dataApp().getCustomProperties(FederatedLearningConfig::class.java)
  }

  companion object {
    private val LOG = LoggerFactory.getLogger(DatasetController::class.java)
  }

  @GetMapping("sparql")
  fun sparql(@RequestParam query: String, @RequestParam format: String, @RequestParam dataset: String) : String {
    
    LOG.debug("received query: " + query)
    LOG.debug("on dataset: " + query)
    LOG.debug("reply format: " + format)
    
    // Create a SPARL query 
    val query = QueryFactory.create(query)
    try {
        // Execute the query on the fuseki server and format the result into the requested format
        QueryExecutionFactory.sparqlService(config.fusekiEndpoint+ "/" + dataset, query).use { qexec ->
            val rs = qexec.execSelect()
            when (format) {
             "XML" -> {
                return ResultSetFormatter.asXMLString(rs);
              }
              "JSON" -> {
                val outputStream =  ByteArrayOutputStream();
                ResultSetFormatter.outputAsJSON(outputStream, rs);
                return String(outputStream.toByteArray());
              }
              else -> {
                val outputStream = ByteArrayOutputStream();
                ResultSetFormatter.outputAsCSV(outputStream, rs);
                return String(outputStream.toByteArray());
              }
            }
        }
    } catch (e: Exception) {
        LOG.info("Error in fuseki: ${e}")
        return "Error in fuseki:" + e
    }

  }
}