package nl.tno.ids.aic.fl.worker

import de.fraunhofer.iais.eis.InvokeOperationMessageBuilder
import de.fraunhofer.iais.eis.ParameterAssignmentBuilder
import de.fraunhofer.iais.eis.util.Util
import nl.tno.ids.aic.fl.DataAppUtils
import nl.tno.ids.aic.fl.DatasetController
import nl.tno.ids.aic.fl.FederatedLearningConfig
import nl.tno.ids.base.IDSRestController
import nl.tno.ids.common.serialization.Config
import nl.tno.ids.common.serialization.DateUtil
import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.StringEntity
import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.*
import java.net.URI
import javax.annotation.PostConstruct
import javax.servlet.http.HttpServletResponse

data class UpdateParameters(val update: String)
data class Worker(val id: String, val idsid: String, val port: Int?, val url: String?)
data class QueryParameters(val workers: List<Worker>, val query: String)

@RestController
@RequestMapping("api")
class SparqlController {
  private var config: FederatedLearningConfig = FederatedLearningConfig()

  @PostConstruct
  fun init() {
    config = Config.dataApp().getCustomProperties(FederatedLearningConfig::class.java)
  }

  companion object {
    private val LOG = LoggerFactory.getLogger(DatasetController::class.java)
  }

  @PostMapping("update")
  fun update(@RequestBody updateParameters: UpdateParameters, response: HttpServletResponse) {
    LOG.info("Received SPARQL update")
    val postRequest = HttpPost(config.updateEndpoint)
    postRequest.setHeader("Content-Type", "application/sparql-update")
    postRequest.entity = StringEntity(updateParameters.update)
    DataAppUtils.executeRequestAndCopyResponse(postRequest, response)
  }

  @PostMapping("query")
  fun get(@RequestBody queryParameters: QueryParameters): List<String> {
    LOG.info("Received SPARQL query: ${queryParameters.workers}")
    return queryParameters.workers.map {
      val invokeOperationMessage = InvokeOperationMessageBuilder()
          ._modelVersion_("2.0.0")
          ._issued_(DateUtil.now())
          ._issuerConnector_(URI.create(Config.dataApp().id))
          ._senderAgent_(URI.create(Config.dataApp().participant))
          ._recipientConnector_(Util.asList(URI.create(it.idsid)))
          ._operationReference_(URI.create("urn:fl:ids:sparql"))
          ._operationParameterization_(ParameterAssignmentBuilder()
              ._parameter_(URI.create("urn:fl:ids:sparql:query"))
              .build())
          .build()

      val result = IDSRestController.sendHTTP(it.idsid, invokeOperationMessage, queryParameters.query)
      LOG.info("Result from ${it.idsid}: ${result.first}")
      result
    }.filter { it.first in 200..299 }.map { it.second.payload }
  }
}