package nl.tno.ids.aic.fl.worker

import de.fraunhofer.iais.eis.*
import de.fraunhofer.iais.eis.util.Util
import nl.tno.ids.aic.fl.FederatedLearningConfig
import nl.tno.ids.aic.fl.IDSController
import nl.tno.ids.base.IDSRestController
import nl.tno.ids.base.MessageHandler
import nl.tno.ids.common.multipart.MultiPart
import nl.tno.ids.common.multipart.MultiPartMessage
import nl.tno.ids.common.serialization.Config
import nl.tno.ids.common.serialization.DateUtil
import nl.tno.ids.common.serialization.SerializationHelper
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import java.net.URI

class ContractRequestMessageHandler : MessageHandler<ContractRequestMessage> {
    private var _dataAppConfig = Config.dataApp()
    private var _customProperties = _dataAppConfig.getCustomProperties(FederatedLearningConfig::class.java)

    companion object {
        private val LOG = LoggerFactory.getLogger(ContractRequestMessageHandler::class.java)
    }

    override fun handle(header: ContractRequestMessage?, payload: String?): ResponseEntity<String>? {
        LOG.info("Parsing Received contract")
        var receivedContractRequest: ContractRequest =
            SerializationHelper.getInstance().fromJsonLD(payload, ContractRequest::class.java)

        LOG.info("Retrieve list of subject data owners refered to in the dataset")
        val dataSetDataOwners = getDatasetDataOwners()

        // Create a list of permission rules that represent the desired permissions that will be sent to the
        // Lawful Ground Manager,.
        var requestPermissions: ArrayList<Permission> = arrayListOf()

        LOG.info("Rebuilding request permission rules with subject data owners")
        // Map data set owner id strings to participants
        val participants =
            ArrayList<Participant>(dataSetDataOwners.map { dataOwner -> ParticipantBuilder(URI.create(dataOwner)).build() })

        // Rebuild the requested permissions with the now known data owner(s) of the dataset.
        for (p in receivedContractRequest.permission) {
            val requestPermission: Permission = PermissionBuilder()
                ._assigner_((participants))
                // Take the consumer from the contractRequest and pass it to provider lawful ground manager
                ._assignee_(ArrayList<Participant>(listOf(ParticipantBuilder(receivedContractRequest.consumer).build())))
                ._constraint_(p.constraint)
                ._targetArtifact_(p.targetArtifact)
                ._targetContent_(p.targetContent)
                .build()
            requestPermissions.add(requestPermission)
        }

        // Rebuild the contract request with the updated rules.
        LOG.info("Rebuilding the Contract request with updated permissions")
        receivedContractRequest = ContractRequestBuilder()
            ._contractStart_(receivedContractRequest.contractStart)
            ._consumer_(receivedContractRequest.consumer)
            ._contractAnnex_(receivedContractRequest.contractAnnex)
            ._contractDate_(receivedContractRequest.contractDate)
            ._contractDocument_(receivedContractRequest.contractDocument)
            ._contractEnd_(receivedContractRequest.contractEnd)
            ._obligation_(receivedContractRequest.obligation)
            ._permission_(requestPermissions)
            ._prohibition_(receivedContractRequest.prohibition)
            ._provider_(receivedContractRequest.provider)
            ._refersTo_(receivedContractRequest.refersTo)
            .build()

        // Forward the updated contract request to the configured Lawful Ground Manager.
        val lawfulGroundManager = _dataAppConfig.getCustomProperties(FederatedLearningConfig::class.java).lawfulGroundManager
        if(lawfulGroundManager == null) {
            LOG.error("No lawful ground manager set, unable to complete contract negotiation process.")
            return IDSController.createRejectionEntity(header, RejectionReason.NOT_FOUND)
        }

        val response = IDSRestController.sendHTTP(
            lawfulGroundManager,
            ContractRequestMessageBuilder()
                ._modelVersion_("3.1.0")
                ._issued_(DateUtil.now())
                ._issuerConnector_(URI.create(_dataAppConfig.id))
                ._senderAgent_(URI.create(_dataAppConfig.id))
                ._recipientConnector_(Util.asList(URI.create(lawfulGroundManager)))
                ._recipientAgent_(Util.asList(URI.create(lawfulGroundManager)))
                .build(),
            SerializationHelper.getInstance().toJsonLD(receivedContractRequest)
        )
        LOG.info(response.toString())

        // Forward the received Contract Offer from the Lawful Ground Manager, to the sender of the ContractRequest
        return MultiPart.toResponseEntity(
            MultiPartMessage.Builder()
                .setHeader(response.second.header)
                .setPayload(response.second.payload)
                .build(), HttpStatus.OK
        )
    }

    /**
     * Get the data owners that the data set refers to
     * For the purpose of this demo, the digit label is the data subject
     */
    private fun getDatasetDataOwners(): List<String> {
        // The ID of this connector is defined in the configuration of the data app.
        return _customProperties.digits
    }
}