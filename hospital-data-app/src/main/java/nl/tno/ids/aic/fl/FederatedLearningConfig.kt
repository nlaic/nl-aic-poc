package nl.tno.ids.aic.fl

class FederatedLearningConfig {
  val resourcePath: String = ""
  val client: Boolean = true
  val datasetEndpoint: String? = null
  val forward: String? = null
  val queryEndpoint: String? = null
  val fusekiEndpoint: String? = null
  val updateEndpoint: String? = null
  val lawfulGroundManager: String? = null
  val digits: List<String> = listOf("1", "2", "3", "4", "5", "6", "7", "8", "9", "0")
}