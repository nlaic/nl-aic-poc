package nl.tno.ids.aic.fl.worker

import de.fraunhofer.iais.eis.InvokeOperationMessage
import de.fraunhofer.iais.eis.RejectionReason
import de.fraunhofer.iais.eis.ResultMessageBuilder
import de.fraunhofer.iais.eis.util.Util
import nl.tno.ids.aic.fl.FederatedLearningConfig
import nl.tno.ids.aic.fl.IDSController
import nl.tno.ids.base.MessageHandler
import nl.tno.ids.common.multipart.MultiPart
import nl.tno.ids.common.multipart.MultiPartMessage
import nl.tno.ids.common.serialization.Config
import nl.tno.ids.common.serialization.DateUtil
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.WebSocket
import okio.ByteString
import org.apache.commons.io.IOUtils
import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.StringEntity
import org.apache.http.impl.client.HttpClients
import org.json.JSONObject
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import java.net.URI
import java.nio.charset.Charset
import java.util.Base64
import java.util.concurrent.ConcurrentHashMap

class WorkerMessageHandler(private val sessions: ConcurrentHashMap<String, WebSocket>) : MessageHandler<InvokeOperationMessage> {
  private val queryEndpoint = Config.dataApp().getCustomProperties(FederatedLearningConfig::class.java).queryEndpoint

  companion object {
    private var LOG = LoggerFactory.getLogger(WorkerMessageHandler::class.java)
  }

  override fun handle(header: InvokeOperationMessage, payload: String?): ResponseEntity<String>? {
    val sessionId = IDSController.uriToSessionId(header.operationParameterization.parameter)
    LOG.debug("Received message: ${header.operationReference} - $sessionId ")
    return when (header.operationReference.toString()) {
      "urn:fl:connect" -> {
        LOG.info("Remote connect $sessionId")
        OkHttpClient.Builder().build().newWebSocket(
            Request.Builder()
                .url(Config.dataApp().customProperties["forward"].asText())
                .build(),
            WorkerWebsocketListener(sessions, sessionId, header.issuerConnector.toString())
        )
        IDSController.createMessageProcessedNotificationEntity(header)
      }
      "urn:fl:binarymessage" -> {
        sessions[sessionId]?.let { ws ->
          LOG.debug("Binary Message: ${payload?.length ?: 0}")
          val bytes = Base64.getDecoder().decode(payload)
          ws.send(ByteString.of(*bytes))
          IDSController.createMessageProcessedNotificationEntity(header)
        } ?: IDSController.createRejectionEntity(header, RejectionReason.NOT_FOUND)
      }
      "urn:fl:message" -> {
        sessions[sessionId]?.let { ws ->
          LOG.debug("Message: ${payload?.length ?: 0}")
          ws.send(payload?.trim() ?: "")
          IDSController.createMessageProcessedNotificationEntity(header)
        } ?: IDSController.createRejectionEntity(header, RejectionReason.NOT_FOUND)
      }
      "urn:fl:close" -> {
        val info = JSONObject(payload)
        sessions[sessionId]?.let { ws ->
          LOG.info("Remote close: ${info.getInt("code")} ${if (info.has("reason")) info.getString("reason") else ""}")
          ws.close(info.getInt("code"), if (info.has("reason")) info.getString("reason") else null)
          IDSController.createMessageProcessedNotificationEntity(header)
        } ?: run {
          LOG.info("Remote close unknown: ${info.getInt("code")} ${if (info.has("reason")) info.getString("reason") else ""}")
          IDSController.createRejectionEntity(header, RejectionReason.NOT_FOUND)
        }
      }
      "urn:fl:ids:sparql" -> {
        LOG.info("Received SPARQL query")
        val post = HttpPost(queryEndpoint)
        post.setHeader("Content-Type", "application/sparql-query")
        post.setHeader("Accept", "text/turtle")
        post.entity = StringEntity(payload)
        val httpClient = HttpClients.custom().disableCookieManagement().build()
        httpClient.execute(post).use { response ->
          if (response.statusLine.statusCode in 200..299) {
            val resultMessage = ResultMessageBuilder()
                ._modelVersion_("2.0.0")
                ._issued_(DateUtil.now())
                ._issuerConnector_(URI.create(Config.dataApp().id))
                ._senderAgent_(URI.create(Config.dataApp().participant))
                ._recipientConnector_(Util.asList(header.issuerConnector))
                ._recipientAgent_(Util.asList(header.senderAgent))
                ._correlationMessage_(header.id)
                .build()
            val multiPartMessage = MultiPartMessage.Builder()
                .setHeader(resultMessage)
                .setPayload(IOUtils.toString(response.entity.content, Charset.defaultCharset()))
                .build()
            MultiPart.toResponseEntity(multiPartMessage, HttpStatus.valueOf(response.statusLine.statusCode))
          } else {
            LOG.info("Error in fuseki: ${IOUtils.toString(response.entity.content, Charset.defaultCharset())}")
            IDSController.createRejectionEntity(header, RejectionReason.INTERNAL_RECIPIENT_ERROR)
          }
        }
      }
      else -> IDSController.createRejectionEntity(header, RejectionReason.INTERNAL_RECIPIENT_ERROR)
    }
  }

}