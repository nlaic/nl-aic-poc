package nl.tno.ids.aic.fl

import com.fasterxml.jackson.databind.ObjectMapper
import de.fraunhofer.iais.eis.*
import de.fraunhofer.iais.eis.util.TypedLiteral
import de.fraunhofer.iais.eis.util.Util
import nl.tno.ids.base.HttpHelper
import nl.tno.ids.common.config.dataapp.DataAppConfig
import nl.tno.ids.common.serialization.Config
import nl.tno.ids.common.serialization.SerializationHelper
import org.apache.http.client.methods.HttpDelete
import org.apache.http.client.methods.HttpGet
import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.ByteArrayEntity
import org.apache.http.entity.StringEntity
import org.apache.http.impl.client.HttpClients
import org.apache.http.util.EntityUtils
import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.*
import java.io.File
import java.net.URI
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*
import javax.annotation.PostConstruct
import javax.servlet.http.HttpServletResponse

data class DataSetWithSubjects(val dataSubjects: List<String>, val datasetName: String)

@RestController
@RequestMapping("api")
class DatasetController() {
  private var federatedLearningConfig: FederatedLearningConfig = FederatedLearningConfig()
  private var dataAppConfig: DataAppConfig = DataAppConfig()
  // TODO: use Data App config
  private val resourceEndpoint = System.getenv("RESOURCES_ENDPOINT")

  @PostConstruct
  fun init() {
    dataAppConfig = Config.dataApp()
    federatedLearningConfig = dataAppConfig.getCustomProperties(FederatedLearningConfig::class.java)
  }

  companion object {
    private val LOG = LoggerFactory.getLogger(DatasetController::class.java)
  }

  @GetMapping("datasets")
  @CrossOrigin(origins = ["*"])
  fun getDatasets(): List<DataSetWithSubjects> {
    return File(federatedLearningConfig.resourcePath).listFiles()?.map { file ->
      var dataSubjects: List<String>
      if(file.name.toLowerCase() == "mnist") {
        dataSubjects = federatedLearningConfig.digits
      }
      else {
        dataSubjects = ArrayList()
      }
      DataSetWithSubjects(dataSubjects, file.name)
    }?.toList() ?: emptyList()
  }

  @PostMapping("datasets/{datasetName}")
  fun postDataset(@PathVariable datasetName: String, @RequestBody dataset: ByteArray, response: HttpServletResponse) {
    LOG.info("received dataset: $datasetName")
    val workerResponseBody = postDatasetToWorker(datasetName, dataset, response)
    persistDatasetAsFile(datasetName, dataset)
    val datasetMetadata = ObjectMapper().readValue(workerResponseBody, DatasetMetadata::class.java)
    val idsResource = convertDatasetMetaDataToIdsResource(datasetMetadata)
    postIdsResourceToCoreContainer(idsResource)
  }

  @DeleteMapping("datasets/{datasetName}")
  fun deleteDataset(@PathVariable datasetName: String, response: HttpServletResponse) {
    LOG.info("Deleting dataset: $datasetName")
    val deleteRequest = HttpDelete("${federatedLearningConfig.datasetEndpoint}/$datasetName")
    deleteRequest.setHeader("Content-Type", "text/csv")
    DataAppUtils.executeRequestAndCopyResponse(deleteRequest, response)
    deleteDatasetFile(datasetName)
  }

  @PostMapping("digits/{datasetName}")
  fun selectDigitsAndPostDataset(@PathVariable datasetName: String, response: HttpServletResponse) {
    LOG.info("Selected MNIST digits: ${federatedLearningConfig.digits} for $datasetName")
    val mnistDataset = downloadMnistAndPrepareDataset(federatedLearningConfig.digits)
    persistDatasetAsFile(datasetName, mnistDataset)
    val workerResponseBody = postDatasetToWorker(datasetName, mnistDataset, response)
    val datasetMetadata = ObjectMapper().readValue(workerResponseBody, DatasetMetadata::class.java)
    val idsResource = convertDatasetMetaDataToIdsResource(datasetMetadata)
    postIdsResourceToCoreContainer(idsResource)
  }

  private fun postIdsResourceToCoreContainer(resource: DataResource) {
    val httpPost = HttpPost(resourceEndpoint)
    httpPost.addHeader("Content-Type", "application/json")
    val resourceJsonLD = SerializationHelper.getInstance().toJsonLD(resource)
    httpPost.entity = StringEntity(resourceJsonLD)
    val response = HttpHelper.getHttpClient().execute(httpPost)
    LOG.info("Posted IDS DataResource to core-container with response: ${response.statusLine.statusCode}")
  }

  private fun convertDatasetMetaDataToIdsResource(datasetMetadata: DatasetMetadata): DataResource {
    return DataResourceBuilder()
        ._title_(Util.asList(TypedLiteral(datasetMetadata.name, "en")))
        // TODO: id or participant?
        ._sovereign_(URI.create(dataAppConfig.participant))
        ._representation_(Util.asList(RepresentationBuilder()
            ._mediaType_(parseMediaType(datasetMetadata.mediaType))
            ._instance_(Util.asList(ArtifactBuilder()
                ._fileName_(datasetMetadata.name)
                ._byteSize_(datasetMetadata.byteSize ?: 0)
                ._creationDate_(datasetMetadata.creationDate)
                .build())
            )
            // TODO: Do we want a Health vocab?
//            ._domainVocabulary_(VocabularyDataBuilder() // Health vocab?
//                .build())
            .build()))
        // TODO: This description doesn't fit with the abstract Resource, but should be placed within the Instance of a Resource.
        ._description_(Util.asList(TypedLiteral(datasetMetadata.shape.toString(), "SHAPE")))
        .build()
  }

  private fun downloadMnistAndPrepareDataset(digitsToTrain: List<String>): ByteArray {
    val mnistUrl = "https://federatedlearning.blob.core.windows.net/datasets/mnist_train.csv"
    var mnistData = ""

    val get = HttpGet(mnistUrl)
    var httpClient = HttpClients.custom().disableCookieManagement().build()
    LOG.info("Downloading MNIST dataset.")
    httpClient.execute(get).use { response ->
      LOG.info("Response dataset: ${response.statusLine.statusCode}")
      response.entity?.let { mnistData = EntityUtils.toString(it) }
    }

    var mnistDataList = mnistData.lines()
    LOG.info("${mnistDataList.size} rows found.")

    // generate headers.
    var headers = "label"
    for (i in 0..783) {
      headers = "$headers,pixel$i"
    }
    headers += "\n"

    LOG.info("Generated headers")
    LOG.info("Filtering dataset.")

    mnistDataList = mnistDataList.filter {
      it.isNotEmpty() && digitsToTrain.contains(it[0].toString())
    }
    LOG.info("Number of rows ${mnistDataList.size}")
    mnistData = mnistDataList.joinToString(separator = "\n")

    mnistData = headers + mnistData
    LOG.info(mnistData.substring(0, 1000))

    return mnistData.toByteArray()
  }

  private fun deleteDatasetFile(datasetName: String) {
    val fileToDelete = Paths.get(federatedLearningConfig.resourcePath, datasetName)
    if (Files.deleteIfExists(fileToDelete)) {
      LOG.info("Deleted dataset $datasetName")
    } else {
      LOG.warn("Could not delete dataset $datasetName")
    }
  }

  private fun persistDatasetAsFile(datasetName: String, dataset: ByteArray) {
    val file = File(federatedLearningConfig.resourcePath, datasetName)
    file.writeBytes(dataset)
    LOG.info("Dataset persisted to ${file.absolutePath}")
  }

  private fun postDatasetToWorker(datasetName: String, dataset: ByteArray, response: HttpServletResponse): String {
    val request = HttpPost("${federatedLearningConfig.datasetEndpoint}/$datasetName")
    request.addHeader("Content-Type", "text/csv")
    val entity = ByteArrayEntity(dataset)
    request.entity = entity
    LOG.info("Posting dataset to worker at ${federatedLearningConfig.datasetEndpoint}/$datasetName")
    return DataAppUtils.executeRequestAndCopyResponse(request, response)
  }

  /**
   * Parse a string media type to IANA media type of info model
   * TODO: Move to common IDS code and add other media types.
   */
  private fun parseMediaType(mediaType: String?): IANAMediaType {
    return when (mediaType) {
      "text/csv" -> IANAMediaType.TEXT_CSV
      "jpg" -> IANAMediaType.IMAGE_JPEG
      "jpeg" -> IANAMediaType.IMAGE_JPEG
      else -> IANAMediaType.TEXT_PLAIN
    }
  }
}