package nl.tno.ids.aic.fl.worker

import de.fraunhofer.iais.eis.InvokeOperationMessage
import de.fraunhofer.iais.eis.RejectionReason
import de.fraunhofer.iais.eis.ArtifactResponseMessageBuilder
import de.fraunhofer.iais.eis.ArtifactRequestMessage

import de.fraunhofer.iais.eis.util.Util
import nl.tno.ids.aic.fl.FederatedLearningConfig
import nl.tno.ids.aic.fl.IDSController
import nl.tno.ids.base.MessageHandler
import nl.tno.ids.common.multipart.MultiPart
import nl.tno.ids.common.multipart.MultiPartMessage
import nl.tno.ids.common.serialization.Config
import nl.tno.ids.common.serialization.DateUtil
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.WebSocket
import okio.ByteString
import org.apache.commons.io.IOUtils
import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.StringEntity
import org.apache.http.impl.client.HttpClients
import org.json.JSONObject
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import java.net.URI
import java.nio.charset.Charset
import java.util.Base64
import java.util.concurrent.ConcurrentHashMap

import org.apache.jena.sparql.engine.http.QueryEngineHTTP
import org.apache.jena.query.ResultSetFormatter
import org.apache.jena.query.QueryExecutionFactory
import org.apache.jena.query.QueryFactory
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule

import nl.tno.ids.common.serialization.SerializationHelper

import java.io.ByteArrayOutputStream;

data class Params(
   var query: String? = null,
   var provider: String? = null, 
   var format: String? = null,
   var dataset: String? = null
  )

class SparqlMessageHandler() : MessageHandler<ArtifactRequestMessage> {
  private val fusekiEndpoint = Config.dataApp().getCustomProperties(FederatedLearningConfig::class.java).fusekiEndpoint

  companion object {
    private var LOG = LoggerFactory.getLogger(SparqlMessageHandler::class.java)
  }

  override fun handle(header: ArtifactRequestMessage, payload: String): ResponseEntity<String>? {
    
    LOG.info("received payload: " + payload)
    val sparqlRequest = ObjectMapper().readValue(payload, Params::class.java)
   
    var queryResponse = ""
    val query = QueryFactory.create(sparqlRequest.query)
    try {
        QueryExecutionFactory.sparqlService(fusekiEndpoint + "/" + sparqlRequest.dataset, query).use { qexec ->
            val rs = qexec.execSelect()
            when (sparqlRequest.format) {
             "XML" -> {
                queryResponse += ResultSetFormatter.asXMLString(rs);
              }
              "JSON" -> {
                val outputStream =  ByteArrayOutputStream();
                ResultSetFormatter.outputAsJSON(outputStream, rs);
                queryResponse += String(outputStream.toByteArray());
              }
              else -> {
                val outputStream = ByteArrayOutputStream();
                ResultSetFormatter.outputAsCSV(outputStream, rs);
                queryResponse += String(outputStream.toByteArray());
              }
            }
        }
    } catch (e: Exception) {
        LOG.info("Error in fuseki:")
        e.printStackTrace()
        return IDSController.createRejectionEntity(header, RejectionReason.INTERNAL_RECIPIENT_ERROR)
    }

    val resultMessage = ArtifactResponseMessageBuilder()
            ._modelVersion_("2.0.0")
            ._issued_(DateUtil.now())
            ._issuerConnector_(URI.create(Config.dataApp().id))
            ._senderAgent_(URI.create(Config.dataApp().participant))
            ._recipientConnector_(Util.asList(header.issuerConnector))
            ._recipientAgent_(Util.asList(header.senderAgent))
            ._correlationMessage_(header.id)
            .build()
    val multiPartMessage = MultiPartMessage.Builder()
        .setHeader(resultMessage)
        .setPayload(queryResponse)
        .build()
    return MultiPart.toResponseEntity(multiPartMessage, HttpStatus.valueOf(200))


  }

}