package nl.tno.ids.aic.government

import com.google.gson.Gson
import de.fraunhofer.iais.eis.*
import de.fraunhofer.iais.eis.util.RdfResource
import de.fraunhofer.iais.eis.util.Util
import nl.tno.ids.base.IDSRestController
import nl.tno.ids.common.serialization.Config
import nl.tno.ids.common.serialization.DateUtil
import nl.tno.ids.common.serialization.SerializationHelper
import org.slf4j.LoggerFactory
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.web.client.RestTemplate
import org.springframework.http.MediaType
import java.net.URI
import java.util.*


data class PermissionRequest(val dataConsumer: String, val dataProvider: String, val dataOwners: List<String>)

class ContractResolver(private val _contractOfferRepository: ContractOfferRepository) : IContractResolver {
    private val _dataAppConfig = Config.dataApp()
    private val _lawfulGroundManagerConfig = _dataAppConfig.getCustomProperties(LawfulGroundManagerConfig::class.java)
    private val legalUrn = "urn:gov:legal"
    private var currentPolicyEvaluationTime: Calendar? = null

    companion object {
        private val LOG = LoggerFactory.getLogger(ContractResolver::class.java)
    }

    override fun resolve(contractRequest: ContractRequest): ContractOffer? {

        LOG.info("Trying to find existing applicable contract.")
        // Check if there is still an applicable contract available.
        for(contractOffer in _contractOfferRepository.get()) {
            if(contractOffer.consumer == contractRequest.consumer
                && contractOffer.provider == contractRequest.provider
                && contractOffer.permission[0].targetArtifact == contractRequest.permission[0].targetArtifact
                && contractOffer.permission[0].targetContent.id == contractRequest.permission[0].targetContent.id) {
                    LOG.info("Applicable contract found, using this contract")
                return contractOffer
            }
        }
        LOG.info("No applicable contract found, attempting to build one.")

        if(contractRequest.permission[0].assigner.isEmpty()) {
            LOG.warn("Contract contains no assigners.")
        }

        
        var rules:List<Permission> = emptyList()

        LOG.info("Creating permission request.")
            val permissionRequest = PermissionRequest(contractRequest.consumer.toString(), contractRequest.provider.toString(),
            contractRequest.permission[0].assigner.map { owner -> owner.id.toString() })
        LOG.info("Permission request created.")

        LOG.info("Rulemanagers configured :${_lawfulGroundManagerConfig.ruleManagers}")

        for(registry in _lawfulGroundManagerConfig.ruleManagers) {
            LOG.info("Contacting Rule Manager: ${registry}")
            val response = IDSRestController.sendHTTP(registry,
                    RequestMessageBuilder()
                            ._modelVersion_("3.1.0")
                            ._issued_(DateUtil.now())
                            ._issuerConnector_(URI.create(_dataAppConfig.id))
                            ._senderAgent_(URI.create(_dataAppConfig.id))
                            ._recipientConnector_(Util.asList(URI.create(registry)))
                            ._recipientAgent_(Util.asList(URI.create(registry)))
                            .build(), Gson().toJson(permissionRequest))
            LOG.info("Response from: ${registry}: ${response.second.payload}")
            LOG.info("Deserialize rules.")
            var resultRules = SerializationHelper.getInstance().fromJsonLDCollection(response.second.payload, Permission::class.java)
            LOG.info("Deserialized rules.")
            rules = rules.plus(resultRules)
        }

        LOG.info("Received all rules. Resolving contract.")

        // Rules for making a contract.
        // 1. Copy any usage constraint directly into the contract offer.
        // 2. Set POLICY_EVALUATION_TIME to lowest time, reject the contract if any POLICY_EVALUATION_TIME has expired.
        // 3. Copy any STATE constraint directly into the contract offer.
        var contractOfferBuilder = ContractOfferBuilder()
        contractOfferBuilder._provider_(contractRequest.provider)
        contractOfferBuilder._consumer_(contractRequest.consumer)

        val contractRequestPermission = contractRequest.permission[0]
        LOG.info("Resolving the following permission request: ${contractRequestPermission}")
        val dataOwners = ArrayList<Participant>(contractRequestPermission.assigner)

        val constraints = ArrayList<Constraint>()

        rules.firstOrNull { rule -> rule.assigner[0].id.toString() == legalUrn }?.let { rule ->
            handleRule(rule, constraints)
            LOG.info("Legal rule found, ignoring data owner rules.")
        } ?: run {
            for(rule in rules) {
                if(!handleRule(rule, constraints)) {
                    LOG.info("Encountered an error while resolving rule for data owner ${rule.assigner[0].id}")
                    return null
                }
                dataOwners.removeIf { participant ->
                    rule.assigner[0].id.toString() == participant.id.toString()
                }
            }
            if(!dataOwners.isEmpty()) {
                LOG.info("Did not find matching rules for data owners ${dataOwners.map { d -> d.id.toString() }.joinToString(",")}")
                return null
            }
        }


        if(currentPolicyEvaluationTime != null) {
            constraints.add(ConstraintBuilder()
                ._leftOperand_(LeftOperand.POLICY_EVALUATION_TIME)
                ._operator_(BinaryOperator.BEFORE)
                ._rightOperand_(RdfResource(DateUtil.asXMLGregorianCalendar(currentPolicyEvaluationTime!!.time).toXMLFormat()))
                .build())
        }

        val contractOffer = ContractOfferBuilder()
            ._contractAnnex_(contractRequest.contractAnnex)
            ._contractDate_(contractRequest.contractDate)
            ._contractDocument_(contractRequest.contractDocument)
            ._contractEnd_(contractRequest.contractEnd)
            ._contractStart_(contractRequest.contractStart)
            ._consumer_(contractRequest.consumer)
            ._obligation_(contractRequest.obligation)
            ._permission_(ArrayList<Permission>(listOf(PermissionBuilder()
                ._assigner_(contractRequestPermission.assigner)
                ._assignee_(contractRequestPermission.assignee)
                ._action_(contractRequestPermission.action)
                ._description_(contractRequestPermission.description)
                ._duty_(contractRequestPermission.duty)
                ._targetArtifact_(contractRequestPermission.targetArtifact)
                ._targetContent_(contractRequestPermission.targetContent)
                ._title_(contractRequestPermission.title)
                ._constraint_(constraints)
                .build())))
            ._prohibition_(contractRequest.prohibition)
            ._provider_(contractRequest.provider)
            ._refersTo_(contractRequest.refersTo)
            .build()

        _contractOfferRepository.addContractOffer(contractOffer)


        LOG.info("Resolved an applicable contract")
        // Send contract offer to Authorization Registry.
        var restTemplate = RestTemplate()
        var headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_JSON
        val request = HttpEntity<String>(SerializationHelper.getInstance().toJsonLD(contractOffer), headers)
        val authorizationRegistryURL = _lawfulGroundManagerConfig.authorizationRegistry + "/api/contracts/"
        restTemplate.postForObject<String>(authorizationRegistryURL, request, String::class.java)
        LOG.info("Contract Offer sent to Authorization Registry")
        return contractOffer
    }

    private fun handleRule(rule: Permission, constraints: ArrayList<Constraint>): Boolean {
        for(constraint in rule.constraint.filterNotNull())
        {
            if(constraint.leftOperand == LeftOperand.PURPOSE)
            {
                if(!constraints.any { con ->
                        con.leftOperand == constraint.leftOperand &&
                                con.operator == constraint.operator &&
                                con.rightOperand == constraint.rightOperand
                    }) {
                    constraints.add(constraint)
                }
            }
            else if(constraint.leftOperand == LeftOperand.POLICY_EVALUATION_TIME)
            {
                var cal1 = javax.xml.bind.DatatypeConverter.parseDateTime(constraint.rightOperand.value.toString())
                val now = GregorianCalendar()


                // Check if policy evaluation time has expired
                if(constraint.operator == BinaryOperator.BEFORE) {
                    if(now >= cal1) {
                        LOG.info("Policy Evaluation Time has expired.")
                        return false
                    }
                }

                if(currentPolicyEvaluationTime == null || currentPolicyEvaluationTime!! > cal1) {
                    currentPolicyEvaluationTime = cal1
                }
            }
            else if(constraint.leftOperand == LeftOperand.ABSOLUTE_SPATIAL_POSITION) {
                if(!constraints.any { con ->
                        con.leftOperand == constraint.leftOperand &&
                                con.operator == constraint.operator &&
                                con.rightOperand == constraint.rightOperand
                    }) {
                    constraints.add(constraint)
                }
            }
        }
        return true
    }
}