/*
 * Copyright 2016 Fraunhofer-Institut für Materialfluss und Logistik IML und Fraunhofer-Institut für Software- und Systemtechnik ISST
 * Industrial Data Space, Reference Use Case Logistics
 *
 * For license information or if you have any questions contact us at ids-ap2@isst.fraunhofer.de.
 */
package nl.tno.ids.aic.government

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.aop.AopAutoConfiguration
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration
import org.springframework.scheduling.annotation.AsyncConfigurer
import org.springframework.scheduling.annotation.EnableAsync
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor
import java.util.concurrent.Executor

/**
 * This is a Spring Boot Application.
 *
 * @see [Spring Boot](http://projects.spring.io/spring-boot/)
 */
@SpringBootApplication(exclude = [DataSourceAutoConfiguration::class, DataSourceTransactionManagerAutoConfiguration::class, HibernateJpaAutoConfiguration::class, AopAutoConfiguration::class])
@EnableAsync
open class Application : AsyncConfigurer {
  override fun getAsyncExecutor(): Executor {
    val executor = ThreadPoolTaskExecutor()
    executor.corePoolSize = 5
    executor.maxPoolSize = 10
    executor.keepAliveSeconds = 5
    executor.initialize()
    return executor
  }

  companion object {
    @JvmStatic
    fun main(args: Array<String>) {
      SpringApplication.run(Application::class.java, *args)
    }
  }
}