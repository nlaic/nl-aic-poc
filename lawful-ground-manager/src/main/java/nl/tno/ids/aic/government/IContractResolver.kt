package nl.tno.ids.aic.government
import de.fraunhofer.iais.eis.ContractOffer
import de.fraunhofer.iais.eis.ContractRequest
import de.fraunhofer.iais.eis.ContractRequestMessage
import de.fraunhofer.iais.eis.ContractResponseMessage

interface IContractResolver {
    fun resolve(contractRequest: ContractRequest): ContractOffer?
}