package nl.tno.ids.aic.government

import de.fraunhofer.iais.eis.ContractOffer
import org.springframework.stereotype.Component


@Component
class ContractOfferRepository {
    private val _contractOffers = mutableListOf<ContractOffer>()

    fun get(id: String): ContractOffer? {
        return _contractOffers.singleOrNull { co -> co.id.toString() == id}
    }

    fun get(): List<ContractOffer> {
        return _contractOffers
    }


    fun addContractOffer(contract: ContractOffer) {
        _contractOffers.add(contract)
    }

    fun deleteContractOffer(id: String) {
        _contractOffers.removeIf { co -> co.id.toString() == id }
    }

    fun deleteContractsWithDataOwner(dataOwnerId: String) {
        if(dataOwnerId == "urn:gov:legal") {
            // Remove all contracts if legal rule changed.
            _contractOffers.clear()
            return
        }
        _contractOffers.removeIf { co->
            co.permission.any { p->
                p.assigner.any { assigner->
                    assigner.id.toString() == dataOwnerId
                }
            }
        }
    }


}