package nl.tno.ids.aic.government.lawfulgroundmanager

import com.google.gson.Gson
import de.fraunhofer.iais.eis.*
import de.fraunhofer.iais.eis.util.Util
import nl.tno.ids.aic.government.LawfulGroundManagerConfig
import nl.tno.ids.aic.government.ContractOfferRepository
import nl.tno.ids.aic.government.ContractResolver
import nl.tno.ids.base.MessageHandler
import nl.tno.ids.common.multipart.MultiPart
import nl.tno.ids.common.multipart.MultiPartMessage
import nl.tno.ids.common.serialization.Config
import nl.tno.ids.common.serialization.DateUtil
import nl.tno.ids.common.serialization.SerializationHelper
import org.slf4j.LoggerFactory
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Component
import org.springframework.web.client.RestTemplate
import java.net.URI

@Component
class ContractRequestMessageHandler(private val _contractOfferRepository: ContractOfferRepository) : MessageHandler<ContractRequestMessage> {
    private var _dataAppConfig = Config.dataApp()
    private val _lawfulGroundManagerConfig = _dataAppConfig.getCustomProperties(LawfulGroundManagerConfig::class.java)

    companion object {
        private val LOG = LoggerFactory.getLogger(ContractRequestMessageHandler::class.java)
    }

    override fun handle(header: ContractRequestMessage?, payload: String?): ResponseEntity<*> {
        LOG.info("Received a contract request message")

        val receivedContractRequest: ContractRequest =
            SerializationHelper.getInstance().fromJsonLD(payload, ContractRequest::class.java)

        LOG.info("Resolving contract...")
        LOG.info(payload)

        // We only need to resolve this contract if it is not in the current list of contract offers.
        val contractOffer = ContractResolver(_contractOfferRepository).resolve(receivedContractRequest)

        if (contractOffer != null) {
            return MultiPart.toResponseEntity(
                MultiPartMessage.Builder()
                    .setHeader(
                        ContractOfferMessageBuilder()
                            ._modelVersion_("3.1.0")
                            ._issued_(DateUtil.now())
                            ._issuerConnector_(URI.create(_dataAppConfig.id))
                            ._senderAgent_(URI.create(_dataAppConfig.id))
                            ._recipientConnector_(Util.asList(header?.issuerConnector))
                            ._recipientAgent_(Util.asList(header?.issuerConnector))
                            .build()
                    )
                    .setPayload(SerializationHelper.getInstance().toJsonLD(contractOffer))
                    .build(), HttpStatus.OK
            )
        } else {
            LOG.info("Contract was rejected")
            return MultiPart.toResponseEntity(
                MultiPartMessage.Builder()
                    .setHeader(
                        ContractRejectionMessageBuilder()
                            ._modelVersion_("3.1.0")
                            ._issued_(DateUtil.now())
                            ._issuerConnector_(URI.create(_dataAppConfig.id))
                            ._senderAgent_(URI.create(_dataAppConfig.id))
                            ._recipientConnector_(Util.asList(header?.issuerConnector))
                            ._recipientAgent_(Util.asList(header?.issuerConnector))
                            .build()
                    )
                    .setPayload("Contract Rejected")
                    .build(), HttpStatus.OK
            )
        }
    }
}
