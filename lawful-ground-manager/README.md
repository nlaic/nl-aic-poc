# Lawful Ground Manager

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 0.1.0](https://img.shields.io/badge/AppVersion-0.1.0-informational?style=flat-square)

This repository contains a Kotlin application for the Lawful Ground Manager. The Lawful Ground Manager takes in Contract Requests and tries to find an applicable contract by contacting each of the defined Rule Managers and merges these rules into an applicable contract.

>*NOTE:* This project is part of a Proof of Concept and therefore does not contain production ready code!

## Building the Docker Image

Gradle is used as build tool, build the docker image by running ./gradlew clean build dockerBuildImage.

## Configuration
This Data App needs some YAML configuration values placed in /ids/config.yaml to work properly:

| Key         | Description                                                           |
| ----------- | --------------------------------------------------------------------- |
| id | URN of the Lawful Ground Manager                            |
| participant | URN of the participant                            |
| customProperties.authorizationRegistry | Endpoint for the authorization registry                            |
| customProperties.ruleManagers | Collection of rule managers URNs                         |
