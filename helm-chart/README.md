# NL AIC Helm charts

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 0.1.0](https://img.shields.io/badge/AppVersion-0.1.0-informational?style=flat-square)

(Documentation generated using [helm-docs](https://github.com/norwoodj/helm-docs))

Umbrella chart for a NL AIC demo

## Deployment content
This umbrella chart deploys the following components using Helm charts:
- IDS broker
- IDS daps
- MongoDb instance
- IDS/SAML hybrid connectors for Hospital1, Hospital2, Hospital3, Researcher and Sparql-consumer.
- SAML-idp demo service

### Preqesition

Before installing the charts the needed dependencies have to be downloaded
```
helm dependency update
```

### Install command
Make sure to install the Helm chart using
```
helm install federated-learning . -f values.yaml -f values.hospital1.yaml -f values.hospital2.yaml -f values.hospital3.yaml -f values.researcher.yaml -f values.sparqlconsumer.yaml
```
to install the connectors for Hospital1, Hospital2, Hospital3, Researcher, Sparql-consumer and the SAML-IdP demo.

### IDS Connectors
Each connector is deployed as a dependency described in ``Chart.yaml``, where an alias is used to give it it's instance name.
For each of those instances, a separate values file (like `values.hospital1.yaml`) is present.

## Configuration values
Most of the values present in the `values*.yaml` files are configured so that all services are connected.
The following values are important to change:
- **mongoDbPassword**: password used to connect to the mongodb instance
- **host**: (in each of the `values.*.yaml`) URL to reach a specific connector.

Documentation and explanation for values are present in `values.hospitl1.yaml`.
Hospital1, Hospital2, Hospital3, Researcher and sparqlconsumer use similar configuration values.

## Docker Registry Pull Secret
For the PoC deployment to work a Docker Pull Secret with the name `ids-pull-secret` has to be present on the Kubernetes cluster in the namespace you'd want to deploy the PoC.

Contact [Maarten Kollenstart](mailto:maarten.kollenstart@tno.nl?SUBJECT=NL%20AIC-PoCs%20-%20Credentials) for more information and the actual credentials.