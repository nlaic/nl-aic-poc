# Researcher Data App

![Version: 0.1.0](https://img.shields.io/badge/Versionz0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 0.1.0](https://img.shields.io/badge/AppVersion-0.1.0-informational?style=flat-square)

This repository contains a Kotlin application for the Researcher data app.

>*NOTE:* This project is part of a Proof of Concept and therefore does not contain production ready code!

## Introduction
This  Data App is used to act as a bridge between the IDS Connector and an external PySyft application used to perform Federated Learning (FL).
The Data App makes it possible to use WebSockets over the IDS network and exposes API endpoints for the front-end.

## Building the Docker Image
Gradle is used as build tool, build the docker image by running ./gradlew clean build dockerBuildImage.

## Configuration
The Data App needs some YAML configuration values placed in /ids/config.yaml to work properly:

| Key         | Description                                                           |
| ----------- | --------------------------------------------------------------------- |
| id | URN of an ID                            |
| participant | URN of the participant                            |
