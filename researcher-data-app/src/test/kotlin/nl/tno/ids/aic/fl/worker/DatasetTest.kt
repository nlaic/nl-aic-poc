package nl.tno.ids.aic.fl.worker

import com.fasterxml.jackson.databind.ObjectMapper
import de.fraunhofer.iais.eis.*
import de.fraunhofer.iais.eis.util.TypedLiteral
import de.fraunhofer.iais.eis.util.Util.asList
import nl.tno.ids.aic.fl.DatasetMetadata
import nl.tno.ids.common.serialization.SerializationHelper
import org.junit.Assert
import org.junit.Test
import java.net.URI
import javax.xml.datatype.DatatypeFactory

class DatasetTest {

  @Test
  fun parseJsonResponseAsObject() {
    val jsonResponse = """
      {"name": "mnist", "shape": [12665, 28, 28], "byteSize": 22862412, "mediaType": "text/csv", "creationDate": "2020-10-02T12:39:08.070902"}
    """.trimIndent()
    val datasetMetadata = ObjectMapper().readValue(jsonResponse, DatasetMetadata::class.java)
    Assert.assertEquals("mnist", datasetMetadata.name)
    Assert.assertEquals(asList(12665, 28, 28), datasetMetadata.shape)
    Assert.assertEquals(2, datasetMetadata.creationDate!!.day)
    Assert.assertEquals(10, datasetMetadata.creationDate!!.month)
    Assert.assertEquals(2020, datasetMetadata.creationDate!!.year)
  }

  @Test
  fun resourceBuildTest() {
    val resource: Resource = DataResourceBuilder()
        ._title_(asList(TypedLiteral("mnist", "en")))
        ._sovereign_(URI.create("me"))
        ._representation_(asList(RepresentationBuilder()
            ._mediaType_(IANAMediaType.TEXT_CSV)
            ._instance_(asList(ArtifactBuilder()
                ._fileName_("lungPhotos-2020.csv")
                ._byteSize_(7000)
                ._creationDate_(DatatypeFactory.newInstance().newXMLGregorianCalendarDate(2020,2,20, 0))
                .build())
            )
//            ._domainVocabulary_(VocabularyDataBuilder() // Health vocab?
//                .build())
            .build()))
        // TODO: This description doesn't fit with the abstract Resource, but should be placed within the Instance of a Resource.
        ._description_(asList(TypedLiteral("(28,28,15000)", "SHAPE")))
        .build()

    val catalog: Catalog = CatalogBuilder()
        ._offer_(asList(resource))
        .build()

    val jsonld: String = SerializationHelper.getInstance().toJsonLD(catalog)
    val parsedResource: Catalog = SerializationHelper.getInstance().fromJsonLD(jsonld, Catalog::class.java)
    println(jsonld)

    Assert.assertNotNull(parsedResource)
  }
}