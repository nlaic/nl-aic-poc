package nl.tno.ids.aic.fl.researcher

import de.fraunhofer.iais.eis.RejectionMessage
import io.javalin.websocket.WsConnectContext
import io.javalin.websocket.WsHandler
import nl.tno.ids.aic.fl.IDSController
import nl.tno.ids.base.IDSRestController
import org.json.JSONObject
import org.slf4j.LoggerFactory
import java.util.Base64
import java.util.concurrent.ConcurrentHashMap
import java.util.function.Consumer

class ResearcherWsHandler(private val sessions: ConcurrentHashMap<String, Pair<WsConnectContext, String>>,
                          private val _contractRepository: ContractRepository) : Consumer<WsHandler> {
  companion object {
    private val LOG = LoggerFactory.getLogger(ResearcherWsHandler::class.java)
  }

  override fun accept(ws: WsHandler) {
    ws.onError { ctx ->
      LOG.error("Websocket Error: ${ctx.sessionId}", ctx.error())
    }
    ws.onConnect { ctx ->
      try {
        ctx.pathParam("forwardTo").let {
          LOG.info("OnConnect forwarding to: $it (${ctx.sessionId})")
          sessions[ctx.sessionId] = Pair(ctx, it)
          IDSRestController.sendHTTP(it, IDSController.headerBuilder("connect", ctx.sessionId, it, _contractRepository), null).let { result ->
            if (result.second.header is RejectionMessage) {
              LOG.error("IDS Rejection: ${(result.second.header as RejectionMessage).rejectionReason}")
            }
          }
        }
      } catch (e: IllegalArgumentException) {
        LOG.error("OnConnect with incorrect forwardTo path parameter: ${ctx.pathParamMap()}")
      }
    }
    ws.onBinaryMessage { messageCtx ->
      LOG.debug("Received binary message on: ${messageCtx.sessionId} (length: ${messageCtx.data().size})")
      messageCtx.session
      sessions[messageCtx.sessionId]?.let {
        IDSRestController.sendHTTP(
            it.second,
            IDSController.headerBuilder("binarymessage", messageCtx.sessionId, it.second, _contractRepository),
            Base64.getEncoder().encodeToString(messageCtx.data().toByteArray())
        ).let { result ->
          if (result.second.header is RejectionMessage) {
            LOG.error("IDS Rejection: ${(result.second.header as RejectionMessage).rejectionReason}")
          }
        }
      }
    }
    ws.onMessage { messageCtx ->
      LOG.debug("Received message on: ${messageCtx.sessionId} (length: ${messageCtx.message().length})")
      sessions[messageCtx.sessionId]?.let {
        IDSRestController.sendHTTP(
            it.second,
            IDSController.headerBuilder("message", messageCtx.sessionId, it.second, _contractRepository),
            messageCtx.message()
        ).let { result ->
          if (result.second.header is RejectionMessage) {
            LOG.error("IDS Rejection: ${(result.second.header as RejectionMessage).rejectionReason}")
          }
        }
      }
    }
    ws.onClose {
      sessions.remove(it.sessionId)?.let { session ->
        LOG.info("Local OnClose for session: ${it.sessionId}: ${it.status()} ${it.reason()}")
        if (it.status() != 1006) {

          IDSRestController.sendHTTP(
              session.second,
              IDSController.headerBuilder("close", it.sessionId, session.second),
              JSONObject()
                  .put("code", it.status())
                  .put("reason", it.reason())
                  .toString()
          ).let { result ->
            if (result.second.header is RejectionMessage) {
              LOG.error("IDS Rejection: ${(result.second.header as RejectionMessage).rejectionReason}")
            }
          }
        }
      } ?: LOG.info("Local OnClose for unknown session: ${it.sessionId}: ${it.status()} ${it.reason()}")

    }
  }
}