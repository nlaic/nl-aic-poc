package nl.tno.ids.aic.fl.researcher

import de.fraunhofer.iais.eis.InvokeOperationMessage
import de.fraunhofer.iais.eis.RejectionReason
import io.javalin.websocket.WsConnectContext
import nl.tno.ids.aic.fl.IDSController
import nl.tno.ids.base.MessageHandler
import org.json.JSONObject
import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import java.nio.ByteBuffer
import java.util.Base64
import java.util.concurrent.ConcurrentHashMap

class ResearcherMessageHandler(private val sessions: ConcurrentHashMap<String, Pair<WsConnectContext, String>>) : MessageHandler<InvokeOperationMessage> {
  companion object {
    private val LOG = LoggerFactory.getLogger(ResearcherMessageHandler::class.java)
  }

  private fun getSession(sessionId: String, idsId: String): WsConnectContext? {
    return sessions[sessionId]?.first ?: sessions.values.firstOrNull { it.second == idsId }?.first
  }

  override fun handle(header: InvokeOperationMessage, payload: String?): ResponseEntity<String>? {
    val sessionId = IDSController.uriToSessionId(header.operationParameterization.parameter)
    LOG.debug("Received message: ${header.operationReference} - $sessionId ")
    return when (header.operationReference.toString()) {
      "urn:fl:connect" -> {
        IDSController.createRejectionEntity(header, RejectionReason.BAD_PARAMETERS)
      }
      "urn:fl:binarymessage" -> {
        getSession(sessionId, header.issuerConnector.toString())?.let {
          it.send(ByteBuffer.wrap(Base64.getDecoder().decode(payload)))
          IDSController.createMessageProcessedNotificationEntity(header)
        } ?: IDSController.createRejectionEntity(header, RejectionReason.NOT_FOUND)
      }
      "urn:fl:message" -> {
        getSession(sessionId, header.issuerConnector.toString())?.let {
          it.send(payload?.trim() ?: "")
          IDSController.createMessageProcessedNotificationEntity(header)
        } ?: IDSController.createRejectionEntity(header, RejectionReason.NOT_FOUND)
      }
      "urn:fl:close" -> {
        val info = JSONObject(payload)
        sessions.remove(sessionId)?.let { session ->
          LOG.info("Remote Close: ${header.issuerConnector} ${info.getInt("code")} ${if (info.has("reason")) info.getString("reason") else ""}")
          session.first.session.close(info.getInt("code"), if (info.has("reason")) info.getString("reason") else null)
          IDSController.createMessageProcessedNotificationEntity(header)
        } ?: run {
          LOG.info("Remote Close unknown: ${header.issuerConnector} ${info.getInt("code")} ${if (info.has("reason")) info.getString("reason") else ""}")
          IDSController.createRejectionEntity(header, RejectionReason.NOT_FOUND)
        }
      }
      else -> IDSController.createRejectionEntity(header, RejectionReason.INTERNAL_RECIPIENT_ERROR)
    }
  }
}