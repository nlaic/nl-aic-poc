package nl.tno.ids.aic.fl.researcher

import de.fraunhofer.iais.eis.ContractOffer
import org.springframework.stereotype.Component


@Component
class ContractRepository() {
    private val _contracts = mutableSetOf<ContractOffer>()

    fun get(): List<ContractOffer> {
        return _contracts.toList()
    }

    fun get(id: String): ContractOffer? {
        return _contracts.singleOrNull { c -> c.id.toString() == id }
    }

    fun getContractForArtifact(artifactId: String): ContractOffer? {
        return _contracts.singleOrNull { c ->
            c.permission.any { p->
                p.targetArtifact.equals(artifactId)
            }
        }
    }
    fun getContractForContent(contentId: String): ContractOffer? {
        return _contracts.singleOrNull { c ->
            c.permission.any { p->
                p.targetContent.id.toString() == contentId
            }
        }
    }

    fun addcontractAgreement(contractAgreement: ContractOffer) {
        _contracts.add(contractAgreement)
    }

    fun deleteContract(id: String) {
        _contracts.removeIf { c -> c.id.toString() == id}
    }

    fun update(contractAgreement: ContractOffer) {
        deleteContract(contractAgreement.id.toString())
        addcontractAgreement(contractAgreement)
    }
}