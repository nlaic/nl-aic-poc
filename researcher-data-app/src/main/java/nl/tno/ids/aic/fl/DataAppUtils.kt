package nl.tno.ids.aic.fl

import org.apache.http.client.methods.HttpUriRequest
import org.apache.http.impl.client.HttpClients
import org.apache.http.util.EntityUtils
import org.slf4j.LoggerFactory
import javax.servlet.http.HttpServletResponse

class DataAppUtils {
  companion object {
    private val LOG = LoggerFactory.getLogger(DataAppUtils::class.java)

    fun executeRequestAndCopyResponse(request: HttpUriRequest, response: HttpServletResponse): String {
      val httpClient = HttpClients.custom().disableCookieManagement().build()
      httpClient.execute(request).use { workerResponse ->
        LOG.info("Worker responded with status code ${workerResponse.statusLine.statusCode}")
        response.status = workerResponse.statusLine.statusCode
        workerResponse.allHeaders.forEach {
          response.addHeader(it.name, it.value)
        }
        if (workerResponse.entity != null) {
          val workerResponseBody = EntityUtils.toString(workerResponse.entity)
          response.writer.write(workerResponseBody)
          return workerResponseBody
        }
      }
      return ""
    }
  }
}
