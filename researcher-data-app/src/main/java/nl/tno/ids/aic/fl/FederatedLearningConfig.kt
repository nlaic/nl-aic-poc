package nl.tno.ids.aic.fl

class FederatedLearningConfig {
  val resourcePath: String = ""
  val client: Boolean = true
  val datasetEndpoint: String? = null
  val forward: String? = null
  val queryEndpoint: String? = null
  val updateEndpoint: String? = null
}