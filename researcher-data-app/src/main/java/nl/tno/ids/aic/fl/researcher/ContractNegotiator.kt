package nl.tno.ids.aic.fl.researcher

import de.fraunhofer.iais.eis.*
import de.fraunhofer.iais.eis.util.RdfResource
import de.fraunhofer.iais.eis.util.Util
import nl.tno.ids.aic.fl.FederatedLearningConfig
import nl.tno.ids.base.IDSRestController
import nl.tno.ids.common.multipart.MultiPartMessage
import nl.tno.ids.common.serialization.Config
import nl.tno.ids.common.serialization.DateUtil
import nl.tno.ids.common.serialization.SerializationHelper
import org.slf4j.LoggerFactory
import java.net.URI

class ContractNegotiator {
    companion object {
        private val LOG = LoggerFactory.getLogger(ContractNegotiator::class.java)
    }
    private var _dataAppConfig = Config.dataApp()
    /**
     * Send a ContractRequest to a DataProvider and receive a MultiPartMessage as response.
     */
    public fun sendContractRequest(targetArtifact: String?, targetContent: String?, dataProvider: String): ContractOffer? {
        LOG.info("Sending ContractRequests for dataProvider: $dataProvider")
        val contractRequest = getContractRequest(targetArtifact, targetContent, dataProvider)
        val response = IDSRestController.sendHTTP(
            dataProvider,
            getContractRequestHeader(dataProvider), SerializationHelper.getInstance().toJsonLD(contractRequest)
        )
        if (response.second.header is ContractOfferMessage) {
            val contractOffer = SerializationHelper.getInstance().fromJsonLD(response.second.payload, ContractOffer::class.java)
            LOG.info("Received ContractOffer for dataProvider: $dataProvider")
            return contractOffer
        }
        else {
            LOG.info("Access to connector $dataProvider was denied.")
            return null
        }
    }

    private fun getContractRequestHeader(dataProvider: String): ContractRequestMessage {
        return ContractRequestMessageBuilder()
            ._modelVersion_("3.1.0")
            ._issued_(DateUtil.now())
            ._issuerConnector_(URI.create(_dataAppConfig.id))
            ._senderAgent_(URI.create(_dataAppConfig.id))
            ._recipientConnector_(Util.asList(URI.create(dataProvider)))
            ._recipientAgent_(Util.asList(URI.create(dataProvider)))
            .build()
    }

    /**
     * Build an IDS ContractRequest for a certain artifact and data provider.
     */
    private fun getContractRequest(targetArtifact: String?, targetContent: String?, dataProvider: String): ContractRequest {
        // Permission rule to use a certain artifact under some constraints.
        val dataConsumerPermission: Permission = PermissionBuilder()
            ._assignee_(ArrayList<Participant>(listOf(ParticipantBuilder(URI.create(_dataAppConfig.id)).build())))
            ._targetArtifact_(targetArtifact?.let { ArtifactBuilder(URI.create(targetArtifact)).build() })
            ._targetContent_(OpaqueContentBuilder(URI.create(targetContent!!)).build())
            ._constraint_(
                ArrayList<Constraint>(
                    listOf(
                        ConstraintBuilder()
                            ._leftOperand_(LeftOperand.PURPOSE)
                            ._operator_(BinaryOperator.EQ)
                            ._rightOperand_(RdfResource("urn:ids:purpose:federated_learning"))
                            .build()
                    )
                )
            )
            .build()

        // Minimal contract request with permission to use a certain artifact.
        // Data Consumer (Researcher) sends this to the data provider (Hospital)
        // Data provider finds the list of data owners associated with this request.
        // Data provider then forwards this contract request together with a list of data owners to the
        // Data Provider Lawful Ground Manager.
        return ContractRequestBuilder()
            ._contractStart_(DateUtil.now())
            ._consumer_(URI.create(_dataAppConfig.id))
            ._provider_(URI.create(dataProvider))
            ._permission_(ArrayList<Permission>(listOf(dataConsumerPermission)))
            .build()
    }
}