package nl.tno.ids.aic.fl.researcher

import nl.tno.ids.common.multipart.MultiPartMessage
import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api/contract-request")
class ContractRequestController(private val _contractRepository: ContractRepository) {
    companion object {
        private val LOG = LoggerFactory.getLogger(ResearcherMessageHandler::class.java)
    }

    @PostMapping("/{artifactId}")
    @CrossOrigin(origins = ["*"])
    fun requestContract(@PathVariable artifactId: String, @RequestBody connectors: List<String>) : List<String> {
        var contractsFound = ArrayList<String>()
        LOG.info("Received contract request.")
        for(connector in connectors) {
            LOG.info("Attempting to find applicable contract for connector $connector")
            ContractNegotiator().sendContractRequest(null, "urn:ids:federated_learning", connector)?.let { contractOffer ->
                _contractRepository.addcontractAgreement(contractOffer)
                contractsFound.add(connector)
            }
        }
        return contractsFound
    }
}