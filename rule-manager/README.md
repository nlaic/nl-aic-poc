# Rule Manager

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 0.1.0](https://img.shields.io/badge/AppVersion-0.1.0-informational?style=flat-square)

This repository contains the Kotlin application for the Rule Manager.

>*NOTE:* This project is part of a Proof of Concept and therefore does not contain production ready code!

## Building the Docker Image

Gradle is used as build tool, build the docker image by running ./gradlew clean build dockerBuildImage.

## Configuration
This Data App needs some YAML configuration values placed in /ids/config.yaml to work properly:

| Key         | Description                                                           |
| ----------- | --------------------------------------------------------------------- |
| id | URN of the Lawful Ground Manager                            |
| participant | URN of the participant                            |
| customProperties.purposes | List of purposes (constraints) that this Rule Manager supports.                          |
| customProperties.targetContent | Target content that this Rule Manager supports i.e. allowed content that can be invoked using InvokeOperationMessage                      |
