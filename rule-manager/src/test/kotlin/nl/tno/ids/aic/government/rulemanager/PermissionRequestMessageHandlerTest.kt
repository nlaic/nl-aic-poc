package nl.tno.ids.aic.government.rulemanager

import de.fraunhofer.iais.eis.RequestMessageBuilder
import kotlinx.serialization.*
import kotlinx.serialization.json.*
import org.junit.Test

class PermissionRequestMessageHandlerTest {

  @Test
  fun handleContractRequest() {
    val ruleRepository = DataOwnerRuleRepository()
    ruleRepository.create("1")
    ruleRepository.addProviderRule("1", DataProviderRule("cons", "prov", true,  true, null, null))
    ruleRepository.addProviderRule("1", DataProviderRule("cons", "prov2", false,  true, null, null))

    val legalRuleRepository = LegalRuleRepository()
    legalRuleRepository.addProviderRule("test", DataProviderRule("cons", "prov", true,  true, null, null))

    val messageHandler = PermissionRequestMessageHandler(ruleRepository, legalRuleRepository)
    val requestMessage = RequestMessageBuilder().build()
    val contractRequest = PermissionRequest("cons", "prov", listOf("1"))

    val result = messageHandler.handle(requestMessage, Json.encodeToString(contractRequest))
    assert(result.statusCode.value() == 200)
  }
}