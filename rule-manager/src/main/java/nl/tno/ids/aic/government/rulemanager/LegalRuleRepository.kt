package nl.tno.ids.aic.government.rulemanager

import org.springframework.stereotype.Component

data class LegalRule(val legalEntity: String, var rulePerProvider: MutableList<DataProviderRule>)

@Component
class LegalRuleRepository {
  private val _legalRule = mutableListOf<LegalRule>()

  fun get(): List<LegalRule> {
    return _legalRule
  }

  fun get(id: String): LegalRule? {
    return _legalRule.singleOrNull { dc -> dc.legalEntity == id }
  }

  fun create(id: String): LegalRule {
    val dataOwnerRule = LegalRule(id, mutableListOf())
    _legalRule.add(dataOwnerRule)
    return dataOwnerRule
  }

  fun addProviderRule(id: String, rule: DataProviderRule) {
    val ownerRule = get(id) ?: create(id)
    ownerRule.rulePerProvider.add(rule)
  }

  fun deleteProviderRule(id: String, rule: DataProviderRule) {
    val ownerRule = get(id) ?: create(id)
    ownerRule.rulePerProvider.removeIf { it.dataConsumer == rule.dataConsumer && it.dataProvider == rule.dataProvider }
  }

  fun update(updatedLegalRule: LegalRule) {
    _legalRule.find { dc -> dc.legalEntity == updatedLegalRule.legalEntity }?.rulePerProvider =
      updatedLegalRule.rulePerProvider
  }
}