package nl.tno.ids.aic.government.rulemanager

import nl.tno.ids.aic.government.RuleManagerConfig
import nl.tno.ids.common.serialization.Config
import org.slf4j.LoggerFactory
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.web.client.RestClientException
import org.springframework.web.client.RestTemplate

class Util {
    companion object {
        private val _ruleManagerConfig = Config.dataApp().getCustomProperties(RuleManagerConfig::class.java)
        private val LOG = LoggerFactory.getLogger(DataOwnerRuleController::class.java)
        fun notifyLawfulGroundManagerOfChanges(dataOwnerId: String ) {
            try {
                var restTemplate = RestTemplate()
                var headers = HttpHeaders()
                val request = HttpEntity<String>("", headers)
                val lawfulGroundManagerURL = _ruleManagerConfig.lawfulGroundManagerAPI + "/${dataOwnerId}"
                LOG.info("Rules changed for data owner: ${dataOwnerId}, notifying Lawful Ground Manager at ${lawfulGroundManagerURL}.")
                restTemplate.postForObject<String>(lawfulGroundManagerURL, request, String::class.java)
            }
            catch (e: RestClientException) {
                LOG.info("Unable to notify Lawful Ground Manager.")
            }
        }
    }
}