package nl.tno.ids.aic.government.rulemanager

import nl.tno.ids.aic.government.RuleManagerConfig
import nl.tno.ids.base.BrokerHandler
import nl.tno.ids.common.serialization.Config
import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("api")
class PurposeController {
    private val _ruleManagerConfig = Config.dataApp().getCustomProperties(RuleManagerConfig::class.java)
    @GetMapping("purposes")
    fun listPurposes(): List<String> {
        return _ruleManagerConfig.purposes
    }

    companion object {
        private val LOG = LoggerFactory.getLogger(ConnectorController::class.java)
    }
}