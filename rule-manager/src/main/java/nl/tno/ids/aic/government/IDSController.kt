package nl.tno.ids.aic.government

import nl.tno.ids.base.IDSRestController
import nl.tno.ids.base.MessageHandler
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.web.bind.annotation.RestController
import javax.annotation.PostConstruct

@EnableScheduling
@RestController
@EnableAutoConfiguration(exclude = [DataSourceAutoConfiguration::class, HibernateJpaAutoConfiguration::class])
open class IDSController(private val _messageHandlers: List<MessageHandler<*>>) : IDSRestController() {
  @PostConstruct
  fun init() {
    LOG.info("Starting IDS Controller, registering message handlers")
    for (messageHandler in _messageHandlers) {
      registerMessageHandler(messageHandler)
    }
  }
}