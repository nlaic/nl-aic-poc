package nl.tno.ids.aic.government.rulemanager

import nl.tno.ids.aic.government.rulemanager.Util.Companion.notifyLawfulGroundManagerOfChanges
import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api/legal-rule")
class LegalRuleController(private val _legalRuleRepository: LegalRuleRepository) {
  companion object {
    private val LOG = LoggerFactory.getLogger(LegalRuleController::class.java)
  }

  @GetMapping()
  fun getRule(): List<LegalRule> {
    return _legalRuleRepository.get()
  }

  @GetMapping("{id}")
  fun getRule(@PathVariable id: String) : LegalRule {
    return _legalRuleRepository.get(id) ?: _legalRuleRepository.create(id)
  }

  @PostMapping("{id}")
  fun updateRule(@PathVariable id: String, @RequestBody legalRule: LegalRule) {
    notifyLawfulGroundManagerOfChanges(id.toString())
    return _legalRuleRepository.update(legalRule)
  }

  @PostMapping("{id}/addProviderRule")
  fun addProviderRule(@PathVariable id: String, @RequestBody providerRule: DataProviderRule) {
    notifyLawfulGroundManagerOfChanges(id.toString())
    return _legalRuleRepository.addProviderRule(id, providerRule)
  }

  @PostMapping("{id}/deleteProviderRule")
  fun deleteProviderRule(@PathVariable id: String, @RequestBody providerRule: DataProviderRule) {
    notifyLawfulGroundManagerOfChanges(id.toString())
    return _legalRuleRepository.deleteProviderRule(id, providerRule)
  }
}