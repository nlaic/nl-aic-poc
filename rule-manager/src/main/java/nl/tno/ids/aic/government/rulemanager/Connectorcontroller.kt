package nl.tno.ids.aic.government.rulemanager

import nl.tno.ids.aic.government.RuleManagerConfig
import nl.tno.ids.base.BrokerHandler
import nl.tno.ids.common.serialization.Config
import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

data class DatasetWithConnectors(val datasetName: String, val connectors: List<String>)

@RestController
@RequestMapping("api")
class ConnectorController {
    private val _ruleManagerConfig = Config.dataApp().getCustomProperties(RuleManagerConfig::class.java)
    @GetMapping("connectors")
    fun listConnectors(): List<String> {
        LOG.info("Received connectors query")
        val query = "PREFIX ids: <https://w3id.org/idsa/core/>\nDESCRIBE * WHERE {\n GRAPH ?g { \n ?s ?o ?p.\n }\n}"
        val connectors = BrokerHandler.getInstance().getConnectors(query)
        // Remove general buildings blocks from output.
        return connectors.filter { connector ->
            !connector.key.contains("LawfulGroundManager")
                && !connector.key.contains("AuthorizationRegistry")
                && !connector.key.contains("RuleManager")
        }.map { connector -> connector.key }
    }

    companion object {
        private val LOG = LoggerFactory.getLogger(ConnectorController::class.java)
    }
}
