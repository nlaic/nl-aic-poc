package nl.tno.ids.aic.government

import nl.tno.ids.aic.government.rulemanager.UsageConstraint

class RuleManagerConfig {
  val resourcePath: String = ""
  val lawfulGroundManagerAPI: String = ""
  val purposes: ArrayList<String> = ArrayList()
  val targetContent: String = ""
  val targetArtifact: String = "data.csv"
}