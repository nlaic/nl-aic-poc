package nl.tno.ids.aic.government.rulemanager

import nl.tno.ids.aic.government.RuleManagerConfig
import nl.tno.ids.aic.government.rulemanager.Util.Companion.notifyLawfulGroundManagerOfChanges
import nl.tno.ids.common.serialization.Config
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.*
import org.springframework.util.MultiValueMap
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate

@RestController
@RequestMapping("api/data-owner-rule")
class DataOwnerRuleController(private val _dataOwnerRuleRepository: DataOwnerRuleRepository) {
  companion object {
    private val LOG = LoggerFactory.getLogger(DataOwnerRuleController::class.java)
  }
  private val _ruleManagerConfig = Config.dataApp().getCustomProperties(RuleManagerConfig::class.java)

  @GetMapping()
  fun getRule(): List<DataOwnerRule> {
    return _dataOwnerRuleRepository.get()
  }

  @GetMapping("{id}")
  fun getRule(@PathVariable id: String) : DataOwnerRule {
    return _dataOwnerRuleRepository.get(id) ?: _dataOwnerRuleRepository.create(id)
  }

  @PostMapping("{id}")
  fun updateRule(@PathVariable id: Int, @RequestBody dataOwnerRule: DataOwnerRule) {
    notifyLawfulGroundManagerOfChanges(id.toString())
    return _dataOwnerRuleRepository.update(dataOwnerRule)
  }

  @PostMapping("{id}/addProviderRule")
  fun addProviderRule(@PathVariable id: String, @RequestBody providerRule: DataProviderRule) {
    notifyLawfulGroundManagerOfChanges(id.toString())
    return _dataOwnerRuleRepository.addProviderRule(id, providerRule)
  }

  @PostMapping("{id}/deleteProviderRule")
  fun deleteProviderRule(@PathVariable id: String, @RequestBody providerRule: DataProviderRule) {
    notifyLawfulGroundManagerOfChanges(id.toString())
    return _dataOwnerRuleRepository.deleteProviderRule(id, providerRule)
  }
}