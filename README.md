# NL AIC PoC

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 0.1.0](https://img.shields.io/badge/AppVersion-0.1.0-informational?style=flat-square)

>*NOTE:* This project is part of a Proof of Concept and therefore does not contain production ready code!

- [PoC description](#poc-description)
  - [Federated Learning (AI-to-data)](#federated-learning-(ai-to-data))
  - [Data access and usage control, contract negotiation and lawful grounding](#data-access-and-usage-control,-contract-negotiation-and-lawful-grounding)
  - [Hybrid data sharing environments](#hybrid-data-sharing-environments)
- [Project structure](#project-structure)
- [Technical Components](#technical-components)
- [Running the demo](#running-the-demo)
  - [ADNI Dataset](#adni-dataset)
  - [ADNI Data](#adni-Data)
  - [Dataset Structure](#dataset-structure)
  - [SAML](#saml)
  - [Federated Learning](#federated-learning)
    - [Preparing the datasets (Hospital1, Hospital2 and Hospital3)](#preparing-the-datasets-hospital1-hospital2-and-hospital3)    
    - [Authorizing the researcher for Federated Learning](#authorizing-the-researcher-for-federated-learning)
    - [Finding available datasets (Researcher)](#finding-available-datasets-researcher)
    - [Start Federated Learning (Researcher)](#start-federated-learning-researcher)

- [Known issues](#known-issues)

This project contains the documentation and code for a Data Space Proof-of-Concept (PoC) based on the IDS approach, built by TNO in association with the Dutch AI Coalition working group "Data Sharing"[(NL-AIC)]( https://nlaic.com/en/bouwsteen/data-sharing/).
The PoC contains an example of a Data Space supporting a multi IdP environment, Feaderated learning, data access and usage control, data access and usage contract negotiation and lawful grounding.

## PoC description

This PoC bundles multiple different use cases of a data space in a single PoC, based on a case in the health care sector. Neverthless, most aspects of this PoC are not restricted to the health care sector and can be applied to any Data Space, regardless of the nature of the organisations involved.

This PoC includes the following use cases:

### Federated Learning (AI-to-data)

This use-case is also implemented in the Health PoC which which be found on [Gitlab](https://gitlab.com/nlaic/health-poc)

Federated Learning (FL) allows for distributed Machine Learning without interchanging privacy sensitive data. How does this work? Suppose you want to train a neural network using data from two parties, let's call them Alice and Bob. Normally you would ask both Alice and Bob to trust you with their data. This is usually a difficult and slow process, especially if the data in question contains privacy sensitive data. Instead of getting the *data* from Alice and Bob to you, Federated Learning sends just the *model* itelf to both Alice and Bob. They then train that model using their own data, and then send the trained model back to you, where you combine the two models back to one model. This can be repeated a number of times to obtain a final model. This way, you can effectively get the same model, but without the data ever needing to leave Alice or Bob, leaving them in control of their own data. 

PySyft is the library that performs Federated Learning, inspired by this [blog post](https://blog.openmined.org/asynchronous-federated-learning-in-pysyft/).

### Data access and usage control, contract negotiation and lawful grounding

This use-case is also implemented in the Public Services PoC and can be found on [Gitlab](https://gitlab.com/nlaic/public-services-poc)

Data Service Providers require a lawful ground to be allowed to share sensitive data. This may be complex, especially when (privacy) sensitive data is involved. Lawful ground can be obtained by means of a permission management process. It covers both processes for defining data sharing rules / policies that provide the legal ground and for  automated translation thereof into machine-interpretable data sharing statements (i.e. a ‘usage contract’ with access and usage control statements) to be used and enforced in individual data transactions. Asking for explicit consent to the Data Subject for sharing privacy-sensitive data for various purposes may be an initial option and may even be the only one under current legal conditions. However, this may not always be the desired approach, e.g. due to complexities for Data Subjects to grasp the essence consequence of the consent being requested and the multitude of consent requests. Therefore, alternative legal policies are expected to emerge as lawful ground for Data Service Providers to share data, e.g. provided by context roles such as the legislator or the domain.
In the system architecture, permission management is enabled by an aligned set of building blocks to support various, case specific, situations and to provide adequate flexibility for handling situations of varying and changing ethical, regulatory and organizational policies. These building blocks adhere to the standardized capabilities in the XACML Policy Framework. The Rule Manager building blocks manage and register the data sharing rules (defined in natural language) for the  the Data Subject, Legislator and Domain. The Lawful Ground Manager retrieves the data sharing rules of the various context roles and translates them into a proposed set of usage contracts. The derived usage contracts are formally approved by the Data Owner (through the Owner Rule Manager) acting as main authority for the Data Service Provider and serve as input for negotiation with Data Service Consumers.  In case the usage contract is formally agreed upon, it is administered in the Authorization Registry to be used in the individual data transactions. The technical enforcement thereof is done by the Policy Enforcement Framework (PEF). The usage contract, the usage rules where it is based upon and the individual data transactions are stored in the Transaction (Accounting) Registry for reporting and conflict resolution.

### Hybrid data sharing environments

This use-case is implemented in the Energy PoC which can be found on [Gitlab](https://gitlab.com/nlaic/energy-poc)

The hybrid data sharing environment poses challenges on interworking in situations in which Data Service Providers and Data Service Consumers have different capabilities. The main goal of this part of the PoC is to demonstrate the possibilities for data sharing for cases in which the Data Service Provider and Data Service Consumer have different data sharing building blocks with respect to identity and authentication.

In this demo 2 types of identification and authentication protocols are used in the connectors:

- IDS, using a hybrid IDS/SAML connector but only configuring the IDS module.
- SAML AND IDS , using a hybrid IDS/SAML connector configuring both the IDS the SAML module.

## Project structure

The PoC consists of several microservices (further explanation [here](#technical-components)).
Some of those services are located as subfolders in this project:

- [Helm Chart](./helm-chart/README.md) contains the Helm code used for deploying the services to a Kubernetes cluster.
- [SPARQL Consuming Data App](./sparql-consuming-data-app/README.md) contains the Java code for the consuming data app, which sends, using IDS components, sparql request to a hospital-data-app.
- [SAML SPARQL UI](./saml-sparql-ui/README.md) contains the SAML/IDS consumer data app and UI. 
- [Researcher Data App](./researcher-data-app/README.md) contains the data app for the researcher
- [Researcher UI](./researcher-ui/README.md) contains the ui for the researcher
- [Hospital Data App](./hospital-data-app/README.md) contains the code for the providing data app and the providing data connector. This data-app is used for all data providers
- [Hospital Gui](./hospital-gui/README.md) contains the code for the providing data ui.
- [Pysyft Researcher](./pysyft-researcher/README.md) Contains the code for the federated learning server
- [Pysyft Worker](./pysyft-worker/README.md) Contains the code for the federated learning worker
- [Rule Manager](./rule-manager/README.md) Contains the data app for the Data subject Rule Manager and the Legal Rule Manager.
- [Rule Manager UI](./rule-manager-gui/README.md) Contains the code for the Rule Manager UI.
- [Lawful Ground Manager](./lawful-ground-manager/README.md) Contains the code for the Lawful Ground Manager data app
- [Authorization Registry](./authorization-registry/README.md) Contains the code for the Authorization Registry data app
- [Authorization Registry UI](./authorization-registry-ui/README.md) Contains the code for the Authorization Registry UI

## Technical Components

The PoC is based on the [IDS reference architecture](https://www.internationaldataspaces.org/wp-content/uploads/2019/03/IDS-Reference-Architecture-Model-3.0.pdf).

![components](img/components.svg)

- Sparql consumer: UI and IDS connector to send SPARQL requests to hospitals supporting SAML or IDS, and display the response
- Hospital 1:  hospital providing sparql querying using SAML authentication and federated learning worker services using IDS.
- Hospital 2 / Hospital 3: 2 hospitals (in the above figure depicted as one component, but deployed as 2 seperate instances) providing SPAQRL querying and feaderated learning worker services using IDS.
- Researcher: A feaderated learning server ui and IDS connector

## Running the demo

The [helm-chart](/helm-chart/README.md) will deploy the demo on a kubernetes cluster.

>*NOTE:* An [Ingress controller](https://kubernetes.io/docs/concepts/services-networking/ingress-controllers/) such as ingress-nginx is required to be able to access any of the services from outside the cluster. Additionally, hostnames have to be defined for each of the hospitals. Refer to your cloud provider for your setup or follow one of the following links to directly go to the Ingress documentation for three common platforms: [Azure](https://docs.microsoft.com/en-us/azure/aks/ingress-basic), [AWS](https://docs.aws.amazon.com/eks/latest/userguide/alb-ingress.html), [Google](https://cloud.google.com/kubernetes-engine/docs/concepts/ingress)



Because the ADNI data is not included in this source the queries will not result is any result.
Data can be aquired at [The Alzheimer's Disease Neuroimaging Initiative (ADNI)](http://adni.loni.usc.edu/data-samples/access-data/).

After deploying the helm charts on a kubernetes cluster, an ADNI dataset has to be created in the fuseki instances.

![fuseki_dataset](img/fuseki_dataset.png)

In order to be able to reach the Fuseki Admin UI when the helm charts are used to deploy the PoC, an admin tool like [Lens](https://k8slens.dev/) can be used. Or an ingress can be created to make the Fuseki Admin UI externally available. 
The dataset has to be created on all Hospital Fuseki instances to be able to use the SPARQL Consumer UI on all these instances.

The standard dataset used in the PoC is "ADNI", and the "Simple Query" tab is created based on this dataset. Another dataset can be queried by using the "Manual Query" tab in the SPARQL consumer UI" and changing the dataset field to the desired dataset.

### ADNI Dataset

Data can be aquired at [The Alzheimer's Disease Neuroimaging Initiative (ADNI)](http://adni.loni.usc.edu/data-samples/access-data/). To be able to access these downloads, permission has to be given by ADNI.

If permission can not be gotten, an example file with fictive data has been added: [Example File](img/example.ttl)
This example ttl file contains one patient with two session( one for protocol1 and one for protocol2). Four tests per session. 

### ADNI Data

The dataset used in this PoC is the ADNI 'Cogstate Battery Results' dataset and is divided over the 3 hospitals in this PoC. The data is transformed to a RDF turtle file using [OpenRefine](https://openrefine.org/) conform the structure depicted in [Dataset Structure](#dataset-structure).


### Dataset Structure

![ttl_graph](img/ttl_graph.svg)

### SAML

One of the IdP's used is [SAML](https://en.wikipedia.org/wiki/SAML_2.0) based.
SAML required the consumer to be authenticated by a SAML IdP and receive an assertion to communicate with the data provider service. Before being able to access data through the SAML based consumer the user must login with the SAML Idp and obtain a SAML assertion. This assertion is then used to send requests to the hybrid IDS Connector to identify and authenticate the consumer.

For this PoC the following credentials are used:

- user: test-user
- password: test password (!Notice the space between test and password)

### Federated Learning

In this walkthrough, we assume that Hospital1, Hospital2, Hospital3 and Researcher connectors are present.
When referencing one of the connectors, the front-end application (UI) is implied. For the purposes of this demo, the MNIST data set is used as a test dataset. This dataset contains images of handwritten digits (0-9) together with ground truth labels. Since this is open data that contains no links to real persons or organisations, personal data is simulated by treating the ground truth labels (0-9) as "user ids", the handwritten image pixel values are then treated as "private data".

>*NOTE:* For Federated Learning we chose to use the MNIST dataset instead of the ADNI dataset, because it is readily available as an open dataset that is free to use. Furthermure, the MNIST dataset does not require a complicated model with a lot of parameters to obtain reasonable accuracy and as such can be used even on modest hardware without GPU's. 

The following steps describe how to setup and execute Federated Learning:

#### Preparing the datasets (Hospital1, Hospital2 and Hospital3)

In each of the worker connectors (Hospital1, Hospital2 and Hospital3):
Click the "Prepare dataset" button. This will download and initialize the MNIST dat set.
Wait until the prepared dataset shows up in the "Datasets" collection.
This step takes a while, because the following actions are performed:

Downloads a MNIST dataset to the connector
Uploads the MNIST dataset to the PySyft worker
Publishes the dataset metadata to the broker

Once this has been completed, the dataset should appear in the list of data sets. This list also shows to which data subjects the data set refers to. For the purpose of this PoC, the digit labels are treated as "user ids" and the image feature vectors as some private data belonging to this person. 

![Preparing a dataset](img/prepare_dataset.png)

#### Authorizing the researcher for Federated Learning

Before Federated Learning can be initiated, A formal contract has to be constructed between the researcher and all three of the hospitals; one for each, resulting in three formal contracts. Before a valid contract can be constructed, the hospitals need a lawful ground to use the MNIST data set for Federated Learning and share the model with the researcher. This lawful ground can be obtained either by explicit consent from the data subjects, or as a legal obligation imposed by a recognized legal entity. In this PoC both are demonstrated. One side note is that in order to obtain a lawful ground for a data usage contract by explicit permission, it is required to obtain permission from *all* the data subjects in the data set, as this PoC features no filtering capabilities. 

For Hospital1, we will add permission rules for all the Data subjects whose data is stored on Hospital1 (recall that Hospital1 contains data for data subjects with ids 1, 2 and 3) such that they give explicit consent for Hospital1 to use their MNIST data for Federated Learning and share the resulting model with the Researcher.

Making these rules can be done as follows:

1. Navigate to the respective url for the Rule Manager UI.
2. use the id of the data subject to log in, for instance 1 for data the subject with id 1.
3. Click "Add rule"
4. The following fields should be set:
    * Data Provider: Hospital1
    * Data Consumer: Researcher
    * Check Allow use for Federated Learning
5. Click "Save changes"

The newly created rule should now appear in the list (see the image below). Repeat the process above for the remaining data subjects (2 and 3).

![Adding rules](img/rule-manager-login.png)

For Hospital2, we will use a legal rule as lawful ground to allow Hospital2 to use data from any user whose data is stored on Hospital2 for Federated Learning and to share the resulting model with the Researcher.

In order to add this legal rule, again navigate to the Rule Manager UI. This time, go to "Legal entity" and login with  "urn:gov:legal" as username. The password field can be ignored. Use the same steps as described above in order to add the legal rule, replacing Hospital1 with Hospital2.

For Hospital3, we will add no rules for now to see how this impacts the Federated Learning process.

#### Finding available datasets (Researcher)

Navigate to the Researcher UI Click "Search available datasets". This queries the broker for all connectors in the network and retrieves the available datasets from each connector.

#### Start Federated Learning (Researcher)

Select the MNIST dataset. You can choose which Hospital connector(s) you would like to use for Federated Learning. Proceed by selecting all the connectors. If necessary, the model parameters (number of epochs etc.) can be fine tuned. Once you are ready, click "Start Federated Learning" in order to start the Federated Learning Process. The following things will happen:

1. The Researcher will ask each Hospital for a Data usage contract. Each Hospital will then ask the Lawful Ground Manager to construct an applicable contract for them. The Lawful Ground Manager uses the rules stored in the Rule Manager to construct this contract. If there are rules missing, this process will fail. and the Lawful Ground Manager will notify the Hospital that no valid contract could be made.
2. Federated Learning will start for those Hospitals that were able to retrieve an applicable contract. In our case, since there are no rules for Hospital3, Hospital3 will not be involved in the Federated Learning process. This is also reflected in the Federated Learning logs.

![Adding rules](img/federated_learning_config.png)

Federated Learning will start, model accuracy updates take place after each epoch.

![Adding rules](img/training.png)

Created contracts can be viewed in the Authorization Registry UI:
![Authorization Registry](img/authorization-registry.png)

Rules can be added for Hospital3 as well so that all three Hospital connectors will be used for Federated Learning, resulting in a better model accuracy.

The SAML IdP is based on the [laa-saml-mock](https://github.com/ministryofjustice/laa-saml-mock) provided through [github](https://github.com) by the UK Ministry of Justice.

The helm-chart uses a simplified version of this SAML IdP, for which a docker image is provided

## Known issues

There are no known issues at this time.
