# Note this is targeted at python 3
import argparse
import tornado.web
import tornado.httpserver
import tornado.ioloop
import tornado.websocket
import tornado.options
import json
import logging
import sys
import asyncio
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import urllib.parse
import threading

import syft as sy
from syft.workers import websocket_client
from syft.frameworks.torch.fl import utils
import nest_asyncio
from websocket import WebSocketConnectionClosedException

LISTEN_PORT = 8080
LISTEN_ADDRESS = '0.0.0.0'

global_workers = []
global_tester = None

"""
Fork of PySyft WebsocketServerWorker (https://github.com/OpenMined/PySyft/blob/master/docker-images/pysyft-worker/worker-server.py)
"""

class ChannelHandler(tornado.websocket.WebSocketHandler):
    def initialize(self, data_app_host: str, test_worker_host: str) -> None:
        self.data_app_host = data_app_host
        self.test_worker_host = test_worker_host
        logger.info(
            "Initialized websocket with data app hostname: {}".format(data_app_host))

    """
    Handler that handles a websocket channel
    """
    @classmethod
    def urls(cls, data_app_host, test_worker_host):
        return [
            # Route/Handler/kwargs
            (r'/ws/', cls, {"data_app_host": data_app_host,
                            "test_worker_host": test_worker_host}),
        ]

    def open(self):
        """
        Client opens a websocket
        """
        logger.info("Researcher Websocket opened")

    def on_message(self, message):
        """
        Message received on channel
        """
        args = json.loads(message)
        args['data_app_host'] = self.data_app_host
        args['test_worker_host'] = self.test_worker_host
        logger.info("Received message: %s", args)
        nest_asyncio.apply()
        asyncio.get_event_loop().run_until_complete(runFederatedLearning(self, args))

    def on_close(self):
        """
        Channel is closed
        """

        try:
            for wcw in global_workers:
                wcw.clear_objects_remote()

            for wcw in global_workers:
                wcw.ws.close()
                logger.info("Closed worker websocket %s.", wcw.id)

        except WebSocketConnectionClosedException:
            logger.error(
                'Error while closing worker socket (most likely the socket was already closed).', exc_info=1)

        if global_tester is not None:
            global_tester.clear_objects_remote()
            global_tester.close()

        logger.info("UI Websocket closed")

    def check_origin(self, origin):
        """
        Override the origin check if needed
        """
        return True

# Loss function


@torch.jit.script
def loss_fn(pred, target):
    return F.nll_loss(input=pred, target=target)

# Model


class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(1, 20, 5, 1)
        self.conv2 = nn.Conv2d(20, 50, 5, 1)
        self.fc1 = nn.Linear(4 * 4 * 50, 500)
        self.fc2 = nn.Linear(500, 10)

    def forward(self, x):
        x = F.relu(self.conv1(x))
        x = F.max_pool2d(x, 2, 2)
        x = F.relu(self.conv2(x))
        x = F.max_pool2d(x, 2, 2)
        x = x.view(-1, 4 * 4 * 50)
        x = F.relu(self.fc1(x))
        x = self.fc2(x)
        return F.log_softmax(x, dim=1)


async def fit_model_on_worker(
    worker: websocket_client.WebsocketClientWorker,
    traced_model: torch.jit.ScriptModule,
    batch_size: int,
    curr_round: int,
    max_nr_batches: int,
    lr: float,
):
    """Send the model to the worker and fit the model on the worker's training data.

    Args:
        worker: Remote location, where the model shall be trained.
        traced_model: Model which shall be trained.
        batch_size: Batch size of each training step.
        curr_round: Index of the current training round (for logging purposes).
        max_nr_batches: If > 0, training on worker will stop at min(max_nr_batches, nr_available_batches).
        lr: Learning rate of each training step.

    Returns:
        A tuple containing:
            * worker_id: Union[int, str], id of the worker.
            * improved model: torch.jit.ScriptModule, model after training at the worker.
            * loss: Loss on last training batch, torch.tensor.
    """
    train_config = sy.TrainConfig(
        model=traced_model,
        loss_fn=loss_fn,
        batch_size=batch_size,
        shuffle=True,
        max_nr_batches=max_nr_batches,
        epochs=1,
        optimizer="SGD",
        optimizer_args={"lr": lr},
    )
    train_config.send(worker)
    loss = await worker.async_fit(dataset_key="mnist", return_ids=[0])
    model = train_config.model_ptr.get().obj
    return worker.id, model, loss


def evaluate_model_on_worker(
    model_identifier: str,
    worker: websocket_client.WebsocketClientWorker,
    dataset_key: str,
    model,
    nr_bins: int,
    batch_size: int,
    websocket: tornado.websocket.WebSocketHandler,
    device="cpu",
    print_target_hist=False,
):
    model.eval()

    # Create and send train config
    train_config = sy.TrainConfig(
        batch_size=batch_size, model=model, loss_fn=loss_fn, optimizer_args=None, epochs=1
    )

    train_config.send(worker)

    result = worker.evaluate(
        dataset_key=dataset_key,
        return_histograms=True,
        nr_bins=nr_bins,
        return_loss=True,
        return_raw_accuracy=True,
    )

    test_loss = result["loss"]
    correct = result["nr_correct_predictions"]
    len_dataset = result["nr_predictions"]

    if print_target_hist:
        logger.info("Target histogram: %s", result["histogram_target"])
        websocket.write_message("Target histogram: %s" %
                                result["histogram_target"])
    hist_pred_log = "%s: Percentage numbers: %s" % (
        model_identifier, [100*x/10 for x in result["histogram_predictions"]])
    logger.info(hist_pred_log)
    websocket.write_message(hist_pred_log)

    loss_log = f"{model_identifier}: Average loss: {test_loss:.4f}, Accuracy: {correct}/{len_dataset} ({100.0 * correct / len_dataset:.2f}%)"
    logger.info(loss_log)
    websocket.write_message(loss_log)


def createClientWorker(worker, host, kwargs_websocket):
    if 'url' in worker:
        logger.info("Creating direct client worker: %s", worker)
        return websocket_client.WebsocketClientWorker(
            id=worker['id'],
            host=worker['url'],
            port=worker['port'],
            **kwargs_websocket)
    else:
        logger.info(
            "Creating indirect client worker: %s, with host %s", worker, host)
        return websocket_client.WebsocketClientWorker(
            id=worker['id'],
            host=host,
            port="8081/"+urllib.parse.quote(worker['idsid']),
            **kwargs_websocket)


async def runFederatedLearning(websocket: tornado.websocket.WebSocketHandler, args):
    """
    args = {
        "verbose": false,
        "workers": [{
            "id": "alice",
            "idsid": "urn:ids:connectors:alice"
        }],
        "seed": 1,
        "lr": 0.1,
        "training_rounds": 40,
        "batch_size": 32,
        "test_batch_size": 32,
        "federate_after_n_batches": 10,
        "save_model": true
    }
    """
    # global because we need to know all workers in over all threads
    global global_workers
    global global_tester
    logger.info("Running federated learning: %s", args)
    hook = sy.TorchHook(torch)
    kwargs_websocket = {"hook": hook, "verbose": args['verbose']}
    websocket.write_message("Starting federated learning")
    workers = map(lambda w: createClientWorker(
        w, args['data_app_host'], kwargs_websocket), args['workers'])
    workers = list(workers)
    global_workers = workers

    logger.info("Workers: %s", workers)

    testing = websocket_client.WebsocketClientWorker(
        id="testing", host=args['test_worker_host'], port="8777", **kwargs_websocket)
    global_tester = testing

    for wcw in workers:
        wcw.clear_objects_remote()

    global_tester.clear_objects_remote()
    torch.manual_seed(args['seed'])
    device = torch.device("cpu")
    model = Net().to(device)
    traced_model = torch.jit.trace(model, torch.zeros(
        [1, 1, 28, 28], dtype=torch.float).to(device))
    learning_rate = args['lr']
    for curr_round in range(1, args['training_rounds'] + 1):
        logger.info("Training round %s/%s", curr_round,
                    args['training_rounds'])
        websocket.write_message("Training round %s/%s" %
                                (curr_round, args['training_rounds']))
        results = await asyncio.gather(
            *[
                fit_model_on_worker(
                    worker=worker,
                    traced_model=traced_model,
                    batch_size=args['batch_size'],
                    curr_round=curr_round,
                    max_nr_batches=args['federate_after_n_batches'],
                    lr=learning_rate,
                )
                for worker in workers
            ]
        )
        logger.info("Results: %s", results)
        models = {}
        loss_values = {}
        test_models = curr_round % 10 == 1 or curr_round == args['training_rounds']
        if test_models:
            logger.info("Evaluating models")
            websocket.write_message("Evaluating models")
            np.set_printoptions(formatter={"float": "{: .0f}".format})
            for worker_id, worker_model, _ in results:
                evaluate_model_on_worker(
                    model_identifier="Model update " + worker_id,
                    worker=testing,
                    dataset_key="mnist_testing",
                    model=worker_model,
                    nr_bins=10,
                    batch_size=args['test_batch_size'],
                    websocket=websocket,
                    print_target_hist=True
                )
        # Federate models (note that this will also change the model in models[0]
        for worker_id, worker_model, worker_loss in results:
            if worker_model is not None:
                models[worker_id] = worker_model
                loss_values[worker_id] = worker_loss
        traced_model = utils.federated_avg(models)
        if test_models:
            evaluate_model_on_worker(
                model_identifier="Federated model",
                worker=testing,
                dataset_key="mnist_testing",
                model=traced_model,
                nr_bins=10,
                batch_size=128,
                websocket=websocket,
                print_target_hist=True
            )
        # decay learning rate
        learning_rate = max(0.98 * learning_rate, args['lr'] * 0.01)
    if args['save_model']:
        torch.save(model.state_dict(), "mnist_cnn.pt")

    websocket.close()


def main():
    # Parse args
    parser = argparse.ArgumentParser(
        description="Run websocket server worker.")
    parser.add_argument(
        "--port",
        "-p",
        type=int,
        help="port number of the websocket server worker, e.g. --port 8080",
        default=8080
    )

    parser.add_argument(
        "--data-app-host",
        "-d",
        type=str,
        help="hostname of ids data app",
        required=True
    )

    parser.add_argument(
        "--test-worker-host",
        "-t",
        type=str,
        help="hostname of the worker that will evaluate the model",
        required=True
    )

    args = parser.parse_args()
    # Create tornado application and supply URL routes
    application = tornado.web.Application(
        ChannelHandler.urls(args.data_app_host, args.test_worker_host))

    # Setup HTTP Server
    http_server = tornado.httpserver.HTTPServer(application)
    http_server.listen(args.port, LISTEN_ADDRESS)

    # Start IO/Event loop
    tornado.ioloop.IOLoop.instance().start()


FORMAT = "%(asctime)s | %(message)s"
logging.basicConfig(format=FORMAT)
logger = logging.getLogger("server")
logger.setLevel(level=logging.DEBUG)

if __name__ == '__main__':
    main()
