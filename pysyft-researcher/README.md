# PySyft Researcher
This repository contains python code that enables Federated Learning using PySyft.
Code from [this blog](https://blog.openmined.org/asynchronous-federated-learning-in-pysyft/) is used as basis.

> *NOTE:* This project is part of a Proof of Concept and therefore does not contain production ready code!


## Introduction
The PySyft Researcher is a python application that hosts a Websocket server and API endpoints using Tornado.
The Researcher acts as a FL Coordinator, it connects to the remote workers and coordinates the FL training algorithm.
This application does know nothing of IDS related concepts, all those responsibilities are handled in the *Researcher Data App*.

## Building the project
This project is built using Docker; ``docker build -t TAG_NAME .``.

### Configuration
The PySyft Researcher needs the following environment variables in order to work properly:

| Key              | Description                                 |
| ---------------- | ------------------------------------------- |
| DATA_APP_HOST    | Endpoint of the Federated Learning Data App |
| TEST_WORKER_HOST | Endpoint of the PySyft evaluator            |
