package nl.tno.nlaic.consumer

import com.fasterxml.jackson.databind.ObjectMapper;
import de.fraunhofer.iais.eis.ArtifactRequestMessage;
import de.fraunhofer.iais.eis.ArtifactRequestMessageBuilder;
import de.fraunhofer.iais.eis.RejectionMessage;
import de.fraunhofer.iais.eis.util.Util
import nl.tno.ids.common.multipart.MultiPartMessage;
import nl.tno.ids.common.serialization.Config;
import nl.tno.ids.common.serialization.DateUtil;
import nl.tno.ids.base.IDSRestController
import org.springframework.data.util.Pair;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*
import org.slf4j.LoggerFactory
import java.net.URI
import com.fasterxml.jackson.module.kotlin.KotlinModule

import java.io.ByteArrayOutputStream;

data class Params(
   var query: String,
   var provider: String, 
   var format: String,
   var dataset: String
  )

@RestController
@RequestMapping("api")
class SAMLSparqlController {
  private val mapper = ObjectMapper().registerModule(KotlinModule())

  companion object {
    private val LOG = LoggerFactory.getLogger(SAMLSparqlController::class.java)
  }

  @GetMapping("sparql")
  fun sparql(@RequestParam query: String, @RequestParam format: String,
             @RequestParam dataset: String, @RequestParam provider: String) : String {
    
    LOG.debug("received query: ${query}")
    LOG.debug(" for: ${provider}")
    LOG.debug(" on datata: ${dataset}")
    LOG.debug(" in format: ${format}")
    
    val sparqlRequest = Params(query,provider,format,dataset);
    sparqlRequest.query=query
    sparqlRequest.provider=provider
    sparqlRequest.format=format
    sparqlRequest.dataset=dataset

    val payload = mapper.writeValueAsString(sparqlRequest);

    LOG.debug("Sending request : ${payload}")
    val artifactRequestMessage =  ArtifactRequestMessageBuilder()
        ._modelVersion_("2.0.0")
        ._issued_(DateUtil.now())
        ._issuerConnector_(URI.create(Config.dataApp().getId()))
        ._recipientConnector_(Util.asList(URI.create(provider)))
        ._requestedArtifact_(URI.create("sparql"))
        .build();

    val result = IDSRestController.sendHTTP(provider, artifactRequestMessage, payload)
    LOG.debug("result first : ${result.first}")
    LOG.debug("result first : ${result.second}")
    return result.second.payload

  }
}