# Rule Manager GUI

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 0.1.0](https://img.shields.io/badge/AppVersion-0.1.0-informational?style=flat-square)

This repository contains a Vue application that acts as the front-end for the Rule Manager.

>*NOTE:* This project is part of a Proof of Concept and therefore does not contain production ready code!

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

## Configuration
The Consumer GUI needs the following environment variables set to work properly

| Key         | Description                                                           |
| ----------- | --------------------------------------------------------------------- |
| API_BACKEND | Endpoint of the Data App API                                          |
