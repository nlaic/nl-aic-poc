package nl.tno.ids.aic.government.authorizationregistry

import de.fraunhofer.iais.eis.*
import org.springframework.stereotype.Component
import java.net.URI
import java.util.*
import kotlin.collections.ArrayList

@Component
class ContractRepository {
    private val _contracts = mutableSetOf<ContractOffer>()

    init {
    }

    fun get(): List<ContractOffer> {
        return _contracts.toList()
    }

    fun get(id: String): ContractOffer? {
        return _contracts.singleOrNull { c -> c.id.toString() == id }
    }

    fun addcontractAgreement(contractAgreement: ContractOffer) {
        _contracts.add(contractAgreement)
    }

    fun deleteContract(id: String) {
        _contracts.removeIf { c -> c.id.toString() == id}
    }

    fun update(contractAgreement: ContractOffer) {
        deleteContract(contractAgreement.id.toString())
        addcontractAgreement(contractAgreement)
    }
}

