# Authorization Registry 

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 0.1.0](https://img.shields.io/badge/AppVersion-0.1.0-informational?style=flat-square)

This repository contains the Kotlin code for the Authorization registry.

>*NOTE:* This project is part of a Proof of Concept and therefore does not contain production ready code!

## Building the Docker Image
Gradle is used as build tool, build the docker image by running ./gradlew clean build dockerBuildImage.

## Configuration
The Data App needs some YAML configuration values placed in /ids/config.yaml to work properly:


| Key         | Description                                                           |
| ----------- | --------------------------------------------------------------------- |
| id          | Endpoint of the Authorization Registry API                            |
| participant | URN of the participant                                                |
